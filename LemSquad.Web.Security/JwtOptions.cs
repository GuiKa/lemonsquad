﻿namespace LemSquad.Web.Security
{
    public class JwtOptions
    {
        public string SecretKey { get; set; }

        public string Issuer { get; set; }

        public string CookieName { get; set; }
    }
}
