﻿namespace LemSquad.Web.Security
{
    public class LoginSettings
    {
        public string Salt { get; set; }
        public int MaxPasswordFail { get; set; }
        public int PasswordFailCooldown { get; set; }
        public int TokenTtl { get; set; }
    }
}
