﻿using System.Collections.Generic;
using System.Security.Claims;

namespace LemSquad.Web.Security
{
    public class IdentityModel
    {
        public string Username { get; set; }

        public IEnumerable<Claim> Claims { get; set; }
    }
}
