﻿using System.Collections.Generic;

namespace LemSquad.Web.Security
{
    public class LoginModel
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public IEnumerable<KeyValuePair<string, string>> ToPostData => new[]
        {
            new KeyValuePair<string, string>(nameof(Username), Username),
            new KeyValuePair<string, string>(nameof(Password), Password)
        };
    }
}
