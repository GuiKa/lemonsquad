﻿using System;

namespace LemSquad.Web.Security
{
    public class TokenModel
    {
        public string Token { get; set; }

        public string Username { get; set; }

        public DateTime ExpirationDate { get; set; }

        public bool IsValid => DateTime.UtcNow < ExpirationDate;
    }
}
