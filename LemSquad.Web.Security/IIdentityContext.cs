﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemSquad.Web.Security
{
    public interface IIdentityContext
    {
        string Token { get; }

        string Username { get; }

        DateTime? ExpirationDate { get; }

        string UserHost { get; }

        string AgentName { get; }

        string AgentVersion { get; }

        string[] Roles { get; }
    }
}
