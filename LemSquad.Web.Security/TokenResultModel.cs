﻿namespace LemSquad.Web.Security
{
    public class TokenResultModel
    {
        public TokenModel Token { get; set; }

        public string Message { get; set; }
    }
}
