﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace LemSquad.Web.Security
{
    public class TokenProvider
    {
        private readonly IIdentityProvider _identityProvider;
        private readonly JwtOptions _jwtOptions;

        public TokenProvider(IIdentityProvider identityProvider, 
            IOptions<JwtOptions> jwtOptions)
        {
            _identityProvider = identityProvider;
            _jwtOptions = jwtOptions.Value;
        }

        public async Task<TokenResultModel> ProvideToken(LoginModel model, int ttl)
        {
            var identity = await _identityProvider.ProvideIdentity(model.Username, model.Password);

            if (identity.Username == null)
                return new TokenResultModel
                {
                    Message = identity.Message
                };

            var start = DateTime.UtcNow;
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.SecretKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expiration = start.AddMinutes(ttl);

            var token = new JwtSecurityToken(_jwtOptions.Issuer,
                _jwtOptions.Issuer,
                expires: expiration,
                signingCredentials: creds, 
                claims: new []
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, identity.Username )
                    }
                    .Concat(identity.Claims));
            
            return new TokenResultModel
            {
                Token = new TokenModel
                {
                    Username = identity.Username,
                    ExpirationDate = expiration,
                    Token = new JwtSecurityTokenHandler().WriteToken(token)
                }
            };
        }
    }
}
