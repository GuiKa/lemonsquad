﻿using System.Collections.Generic;
using System.Security.Claims;

namespace LemSquad.Web.Security
{
    public class ProvidedIdentityModel
    {
        public string Username { get; set; }

        public string Message { get; set; }

        public IEnumerable<Claim> Claims { get; set; }
    }
}
