﻿using System.Threading.Tasks;

namespace LemSquad.Web.Security
{
    public interface IIdentityProvider
    {
        Task<ProvidedIdentityModel> ProvideIdentity(string username, string password);
    }
}
