﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LemonSquad.Models
{
    public class AccountToken
    {
        public AccountToken()
        {
            Value = RandomString(10);
            CreationDate = DateTime.UtcNow;
            Type = TokenType.Register;
            Admin = false;
        }

        public int AccountTokenId { get; set; }

        public string Value { get; set; }

        public TokenType Type { get; set; }

        public bool Admin { get; set; }

        public DateTime CreationDate { get; set; }

        public int? AccountId { get; set; }
        public virtual Account Account { get; set; }

        public static string RandomString(int length)
        {
            var random = new Random();
            return string.Join("", Enumerable.Range(0, length)
                .Select(x => ((char) random.Next(65, 123)).ToString()));
        }
    }

    public enum TokenType
    {
        Register=1
    }
}
