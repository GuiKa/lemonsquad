﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Models
{
    public class Image
    {
        public int ImageId { get; set; }

        public string Extension { get; set; }

        public string Hash { get; set; }

        public byte[] Content { get; set; }
    }
}
