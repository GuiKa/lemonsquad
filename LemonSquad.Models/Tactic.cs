﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Models
{
    public sealed class Tactic
    {
        public Tactic()
        {
            CreationDate = DateTime.UtcNow;
            Status = TacticStatus.Closed;
            Team = Team.Marsoc;
            TacticContents = new List<TacticContent>();
            TacticComments = new List<TacticComment>();
        }

        public int TacticId { get; set; }

        public TacticStatus Status { get; set; }

        public Team Team { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime CreationDate { get; set; }

        public int MapUplinkId { get; set; }
        public MapUplink MapUplink { get; set; }

        public int AccountId { get; set; }
        public Account Account { get; set; }

        public ICollection<TacticContent> TacticContents { get; set; }
        public ICollection<TacticComment> TacticComments { get; set; }
    }

    public enum TacticStatus
    {
        Closed=0,
        Opened=1
    }

    public enum Team
    {
        Marsoc = 0,
        Volk = 1
    }
}
