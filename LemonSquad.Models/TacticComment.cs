﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Models
{
    public class TacticComment
    {
        public TacticComment()
        {
            CreationDate = DateTime.UtcNow;
        }

        public int TacticCommentId { get; set; }

        public string Content { get; set; }

        public DateTime CreationDate { get; set; }

        public int TacticId { get; set; }
        public virtual Tactic Tactic { get; set; }

        public int AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}
