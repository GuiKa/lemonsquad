﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Models
{
    public class RoleModel
    {
        public int RoleId { get; set; }

        public string Name { get; set; }
    }
}
