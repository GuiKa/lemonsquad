﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Models
{
    public class AccountRole
    {
        public int AccountRoleId { get; set; }

        public int RoleId { get; set; }
        public virtual Role Role { get; set; }

        public int AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}
