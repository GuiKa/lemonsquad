﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Models
{
    public class TacticContent
    {
        public int TacticContentId { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public int TacticId { get; set; }
        public virtual Tactic Tactic { get; set; }

        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }
    }
}
