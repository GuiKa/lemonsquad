﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Models
{
    public class MapUplink
    {
        public int MapUplinkId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }

        public int MapId { get; set; }
        public virtual Map Map { get; set; }

        public virtual IEnumerable<Tactic> Tactics { get; set; }
    }
}
