﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Models
{
    public class Account
    {
        public Account()
        {
            CreationDate = DateTime.UtcNow;
            LastPasswordFail = DateTime.UtcNow;
            Enabled = true;
        }

        public int AccountId { get; set; }

        public bool Enabled { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public int PasswordFails { get; set; }

        public DateTime LastPasswordFail { get; set; }

        public DateTime CreationDate { get; set; }

        public virtual ICollection<Tactic> Tactics { get; set; }
        public virtual ICollection<AccountToken> AccountTokens { get; set; }
        public virtual ICollection<TacticComment> TacticComments { get; set; }
        public virtual ICollection<AccountRole> AccountRoles { get; set; }
    }
}
