﻿using System;
using System.Collections.Generic;
using System.Text;
using LemonSquad.Models;

namespace LemonSquad.Web.Models
{
    public class TacticContentModel
    {
        public int TacticContentId { get; set; }

        public int TacticId { get; set; }
        
        public string Title { get; set; }

        public string Content { get; set; }

        public int? ImageId { get; set; }
    }
}
