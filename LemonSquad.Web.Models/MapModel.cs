﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Web.Models
{
    public class MapModel
    {
        public int MapId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}
