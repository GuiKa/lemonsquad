﻿using System;
using System.Collections.Generic;
using System.Text;
using LemonSquad.Models;

namespace LemonSquad.Web.Models
{
    public class CurrentUserModel
    {
        public DateTime? ExpirationDate { get; set; }

        public AccountModel Account { get; set; }

        public IEnumerable<RoleModel> Roles { get; set; }
    }
}
