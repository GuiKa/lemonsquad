﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Web.Models
{
    public class SeedModel
    {
        public Dictionary<int, MapModel> Maps { get; set; }
        public Dictionary<int, MapUplinkModel> MapUplinks { get; set; }
    }
}
