﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Web.Models
{
    public class ImageModel
    {
        public int ImageId { get; set; }

        public string Extension { get; set; }
    }
}
