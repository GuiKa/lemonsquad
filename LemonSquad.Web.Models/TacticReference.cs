﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Web.Models
{
    public class TacticReference
    {
        public string Name { get; set; }

        public string MapName { get; set; }
    }
}
