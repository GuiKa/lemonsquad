﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Web.Models
{
    public class AccountModel
    {
        public AccountModel()
        {
            Enabled = true;
        }

        public int AccountId { get; set; }

        public bool Enabled { get; set; }

        public string Username { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
