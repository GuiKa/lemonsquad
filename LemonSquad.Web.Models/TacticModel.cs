﻿using System;
using System.Collections.Generic;
using System.Text;
using LemonSquad.Models;

namespace LemonSquad.Web.Models
{
    public class TacticModel
    {
        public TacticModel()
        {
            Status = TacticStatus.Closed;
            TacticContents = new List<TacticContentModel>();
        }

        public int TacticId { get; set; }

        public TacticStatus Status { get; set; }

        public Team Team { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime CreationDate { get; set; }

        public int MapId { get; set; }

        public int MapUplinkId { get; set; }

        public int AccountId { get; set; }
        public virtual AccountModel Account { get; set; }

        public virtual ICollection<TacticContentModel> TacticContents { get; set; }
    }
}
