﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Web.Models
{
    public class MapUplinkModel
    {
        public int MapUplinkId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public int? ImageId { get; set; }
        public virtual ImageModel Image { get; set; }

        public int MapId { get; set; }
    }
}
