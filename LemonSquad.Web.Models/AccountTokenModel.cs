﻿using System;
using System.Collections.Generic;
using System.Text;
using LemonSquad.Models;

namespace LemonSquad.Web.Models
{
    public class AccountTokenModel
    {
        public int AccountTokenId { get; set; }

        public string Value { get; set; }

        public TokenType Type { get; set; }

        public AccountModel Account { get; set; }

        public DateTime CreationDate { get; set; }

    }
}
