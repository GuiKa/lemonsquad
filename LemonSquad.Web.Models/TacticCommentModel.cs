﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LemonSquad.Web.Models
{
    public class TacticCommentModel
    {
        public int TacticCommentId { get; set; }

        public string Content { get; set; }

        public DateTime CreationDate { get; set; }

        public int TacticId { get; set; }

        public int AccountId { get; set; }
        public virtual AccountModel Account { get; set; }
    }
}
