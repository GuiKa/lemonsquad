﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using LemonSquad.Models;
using Microsoft.EntityFrameworkCore;

namespace LemonSquad.Context
{
    public class LsContext : DbContext
    {
        public LsContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountRole> AccountRoles { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Map> Maps { get; set; }
        public DbSet<MapUplink> MapUplinks { get; set; }
        public DbSet<Tactic> Tactics { get; set; }
        public DbSet<TacticContent> TacticContents { get; set; }
        public DbSet<TacticComment> TacticComments { get; set; }
        public DbSet<AccountToken> AccountTokens { get; set; }

        private void EnsureEntitySeed<TEntity>(
            TEntity[] entities,
            DbSet<TEntity> dbSet,
            Expression<Func<TEntity, string>> propSelector)
            where TEntity : class
        {
            foreach (var entity in entities)
            {
                var predicate = Expression.Lambda<Func<TEntity, string>>(
                    Expression.Convert(propSelector.Body, typeof(string)),
                    propSelector.Parameters
                );

                if (!dbSet.Any(et => predicate.Compile()(et) == predicate.Compile()(entity)))
                    dbSet.Add(entity);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TacticComment>()
                .HasOne(x => x.Account)
                .WithMany(x => x.TacticComments)
                .HasForeignKey(x => x.AccountId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TacticComment>()
                .HasOne(x => x.Tactic)
                .WithMany(x => x.TacticComments)
                .HasForeignKey(x => x.TacticId)
                .OnDelete(DeleteBehavior.Restrict);

            base.OnModelCreating(modelBuilder);
        }

        public void EnsureSeedData()
        {
            EnsureEntitySeed(new[]
            {
                new Map
                {
                    Name = "Downfall 1",
                    Code = "DF1",
                    Description = "A big deserted map, good for snipers.",
                    MapUplinks = new List<MapUplink>
                    {
                        new MapUplink
                        {
                            Name = "North HQ",
                            Code = "DNH"
                        },
                        new MapUplink
                        {
                            Name = "Helicopter",
                            Code = "DHP"
                        },
                        new MapUplink
                        {
                            Name = "South HQ",
                            Code = "DSH"
                        },
                        new MapUplink
                        {
                            Name = "South Guard",
                            Code = "DSG"
                        }
                    }
                },
                new Map
                {
                    Name = "Downfall 2",
                    Code = "DF2",
                    Description = "A big deserted map at night, nobody plays this one.",
                    MapUplinks = new List<MapUplink>
                    {
                        new MapUplink
                        {
                            Name = "North HQ",
                            Code = "DNH"
                        },
                        new MapUplink
                        {
                            Name = "Helicopter",
                            Code = "DHP"
                        },
                        new MapUplink
                        {
                            Name = "South HQ",
                            Code = "DSH"
                        },
                        new MapUplink
                        {
                            Name = "South Guard",
                            Code = "DSG"
                        }
                    }
                },
                new Map
                {
                    Name = "Suburbia 1",
                    Code = "SB1",
                    Description = "Small and random, the favorite noob's map.",
                    MapUplinks = new List<MapUplink>
                    {
                        new MapUplink
                        {
                            Name = "North west",
                            Code = "SNW"
                        },
                        new MapUplink
                        {
                            Name = "North east",
                            Code = "SNE"
                        },
                        new MapUplink
                        {
                            //It's not a mansion dam Americans! Have you ever seen mansion? it's huge.
                            Name = "Middle house",
                            Code = "SMH"
                        },
                        new MapUplink
                        {
                            Name = "Burning house",
                            Code = "SBH"
                        },
                        new MapUplink
                        {
                            Name = "South spawn",
                            Code = "SSS"
                        }
                    }
                },
                new Map
                {
                    Name = "Suburbia 2",
                    Code = "SB2",
                    Description = "Small and even more random, the favorite noob's map.",
                    MapUplinks = new List<MapUplink>
                    {
                        new MapUplink
                        {
                            Name = "North west",
                            Code = "SNW"
                        },
                        new MapUplink
                        {
                            Name = "North east",
                            Code = "SNE"
                        },
                        new MapUplink
                        {
                            //It's not a mansion dam Americans! Have you ever seen mansion? it's huge.
                            Name = "Middle house",
                            Code = "SMH"
                        },
                        new MapUplink
                        {
                            Name = "Burning house",
                            Code = "SBH"
                        },
                        new MapUplink
                        {
                            Name = "South spawn",
                            Code = "SSS"
                        }
                    }
                },
                new Map
                {
                    Name = "Quarantine 1",
                    Code = "QT1",
                    Description = "Lemon's city",
                    MapUplinks = new List<MapUplink>
                    {
                        new MapUplink
                        {
                            Name = "North",
                            Code = "QNO"
                        },
                        new MapUplink
                        {
                            Name = "Roof",
                            Code = "QRF"
                        },
                        new MapUplink
                        {
                            Name = "Burning building",
                            Code = "QBB"
                        },
                        new MapUplink
                        {
                            Name = "West courtyard",
                            Code = "QWC"
                        },
                        new MapUplink
                        {
                            Name = "Gas station",
                            Code = "QGS"
                        },
                        new MapUplink
                        {
                            Name = "Tank",
                            Code = "QTN"
                        },
                        new MapUplink
                        {
                            Name = "Ambulance",
                            Code = "QAB"
                        },
                        new MapUplink
                        {
                            Name = "East",
                            Code = "QEA"
                        },
                    }
                },
                new Map
                {
                    Name = "Quarantine 2",
                    Code = "QT2",
                    Description = "Lemon's city at night. Cheezy this one.",
                    MapUplinks = new List<MapUplink>
                    {
                        new MapUplink
                        {
                            Name = "North",
                            Code = "QNO"
                        },
                        new MapUplink
                        {
                            Name = "Roof",
                            Code = "QRF"
                        },
                        new MapUplink
                        {
                            Name = "Burning building",
                            Code = "QBB"
                        },
                        new MapUplink
                        {
                            Name = "West courtyard",
                            Code = "QWC"
                        },
                        new MapUplink
                        {
                            Name = "Gas station",
                            Code = "QGS"
                        },
                        new MapUplink
                        {
                            Name = "Tank",
                            Code = "QTN"
                        },
                        new MapUplink
                        {
                            Name = "Ambulance",
                            Code = "QAB"
                        },
                        new MapUplink
                        {
                            Name = "East",
                            Code = "QEA"
                        },
                    }
                },
                new Map
                {
                    Name = "Tanker",
                    Code = "TNK",
                    Description = "Nobody likes it but it will be in the tournament."
                },
                new Map
                {
                    Name = "Subway",
                    Code = "SBW",
                    Description = "Probably balanced by Riot.",
                    MapUplinks = new List<MapUplink>
                    {
                        new MapUplink
                        {
                            Name = "North deadend",
                            Code = "WNO"
                        },
                        new MapUplink
                        {
                            Name = "Double down",
                            Code = "WDD"
                        },
                        new MapUplink
                        {
                            Name = "Market",
                            Code = "WMK"
                        },
                        new MapUplink
                        {
                            Name = "Center subway",
                            Code = "WCS"
                        },
                        new MapUplink
                        {
                            Name = "South subway",
                            Code = "WSS"
                        }
                    }
                },
                new Map
                {
                    Name = "Bazaar",
                    Code = "BAZ",
                    Description = "A really hard map to deal with but it's fun. Good design.",
                    MapUplinks = new List<MapUplink>
                    {
                        new MapUplink
                        {
                            Name = "West market",
                            Code = "BWM"
                        },
                        new MapUplink
                        {
                            Name = "Center market",
                            Code = "BCM"
                        },
                        new MapUplink
                        {
                            Name = "East market",
                            Code = "BEM"
                        },
                        new MapUplink
                        {
                            Name = "Middle blue room",
                            Code = "BMB"
                        }
                    }
                }
            }, Maps, et => et.Code);

            EnsureEntitySeed(new[]
            {
                new AccountToken
                {
                    Type = TokenType.Register,
                    Value = "GUIKA",
                    Admin = true
                } 
            }, AccountTokens, x => x.Value);


            EnsureEntitySeed(new[]
            {
                new Role()
                {
                    Name = "Admin"
                }
            }, Roles, x => x.Name);

            SaveChanges();
        }
    }
}
