﻿using System;
using System.Linq;
using LemSquad.Web.Security;
using Microsoft.AspNetCore.Http;
using UAParser;

namespace LemonSquad.Web.WebApp.Security
{
    public class UserIdentity : IIdentityContext
    {
        public UserIdentity(IHttpContextAccessor contextAccessor)
        {
            if (contextAccessor?.HttpContext == null)
                return;

            Username = contextAccessor.HttpContext.User.Claims.FirstOrDefault(cl => cl.Type.Contains("nameidentifier"))?.Value;
            var expiration = contextAccessor.HttpContext.User.Claims.FirstOrDefault(cl => cl.Type == "exp")?.Value;
            if (long.TryParse(expiration, out long expirationUnix))
            {
                ExpirationDate = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).AddSeconds(expirationUnix);
            }

            Roles = contextAccessor.HttpContext.User.Claims.Where(cl => cl.Type.Contains("role"))
                .Select(cl => cl.Value).ToArray();

            UserHost = contextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            var userAgentString = contextAccessor.HttpContext.Request.Headers.ContainsKey("User-Agent")
                ? contextAccessor.HttpContext.Request.Headers["User-Agent"].ToString()
                : null;

            if (string.IsNullOrEmpty(userAgentString)) return;
            var uaParser = Parser.GetDefault();
            try
            {
                var userAgent = uaParser.ParseUserAgent(userAgentString);

                AgentName = userAgent.Family;
                AgentVersion = $"{userAgent.Major}.{userAgent.Minor}";
            }
            catch
            {
                // ignored
            }
        }

        public string Token { get; set; }

        public string Username { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string UserHost { get; set; }

        public string AgentName { get; set; }

        public string AgentVersion { get; set; }

        public string[] Roles { get; set; }
    }
}
