﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using LemonSquad.Context;
using LemSquad.Web.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace LemonSquad.Web.WebApp.Security
{
    public class LsIdentityProvider : IIdentityProvider
    {
        private readonly LsContext _db;
        private readonly LoginSettings _loginSettings;
        private readonly IIdentityContext _identityContext;

        public LsIdentityProvider(LsContext db,
            IIdentityContext identityContext,
            IOptions<LoginSettings> loginSettings)
        {
            _db = db;
            _loginSettings = loginSettings.Value;
            _identityContext = identityContext;
        }

        public async Task<ProvidedIdentityModel> ProvideIdentity(string username, string password)
        {
            var account = await _db.Accounts
                .Where(x => x.Enabled)
                .Include(x => x.AccountRoles).ThenInclude(x => x.Role)
                .FirstOrDefaultAsync(ac => ac.Username == username);

            if (account == null)
                return new ProvidedIdentityModel
                {
                    Message = "Wrong username"
                };

            if (account.PasswordFails > _loginSettings.MaxPasswordFail &&
                DateTime.UtcNow < account.LastPasswordFail.AddSeconds(_loginSettings.PasswordFailCooldown))
                return new ProvidedIdentityModel
                {
                    Message = "Blocked"
                };

            if (account.Password != HashPassword(password, _loginSettings.Salt))
            {
                account.LastPasswordFail = DateTime.UtcNow;
                account.PasswordFails++;
                await _db.SaveChangesAsync();
                return new ProvidedIdentityModel
                {
                    Message = "Wrong password"
                };
            }

            var jti = Guid.NewGuid();
            account.PasswordFails = 0;
            await _db.SaveChangesAsync();

            var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Jti, jti.ToString())
                }.Concat(account.AccountRoles.Select(x => new Claim("role", x.Role.Name))).ToArray();
            return new ProvidedIdentityModel
            {
                Username = account.Username,
                Claims = claims
            };
        }

        public static string HashPassword(string pass, string salt)
        {
            var bytes = Encoding.Unicode.GetBytes(pass);
            var src = Convert.FromBase64String(salt);
            var dst = new byte[src.Length + bytes.Length];
            byte[] inArray = null;
            Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
            var algorithm = SHA512.Create();
            inArray = algorithm.ComputeHash(dst);
            return Convert.ToBase64String(inArray);
        }
    }
}
