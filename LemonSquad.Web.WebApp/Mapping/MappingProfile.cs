﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LemonSquad.Models;
using LemonSquad.Web.Models;

namespace LemonSquad.Web.WebApp.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Map, MapModel>();
            CreateMap<MapUplink, MapUplinkModel>();
            CreateMap<Image, ImageModel>();
            CreateMap<Tactic, TacticModel>()
                .ForMember(x => x.MapId, mo => mo.MapFrom(x => x.MapUplink.MapId))
                ;
            CreateMap<TacticContent, TacticContentModel>();

            CreateMap<AccountToken, AccountTokenModel>();
            CreateMap<AccountTokenModel, AccountToken>()
                .ForMember(x => x.AccountTokenId, mo => mo.Ignore())
                .ForMember(x => x.CreationDate, mo => mo.Ignore())
                ;

            CreateMap<TacticModel, Tactic>()
                .ForMember(x => x.TacticId, mo => mo.Ignore())
                .ForMember(x => x.Account, mo => mo.Ignore())
                .ForMember(x => x.AccountId, mo => mo.Ignore())
                .ForMember(x => x.CreationDate, mo => mo.Ignore())
                .ForMember(x => x.TacticContents, mo => mo.Ignore())
                ;
            CreateMap<TacticContentModel, TacticContent>()
                .ForMember(x => x.TacticContentId, mo => mo.Ignore())
                ;

        }
    }
}
