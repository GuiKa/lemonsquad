﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LemonSquad.Web.WebApp.Filter
{
    public class HasRolesFilterFactory : Attribute, IFilterFactory
    {
        public string[] Roles { get; set; } = new string[] { };

        public RolesBehaviour RolesBehaviour { get; set; } = RolesBehaviour.And;

        public bool IsReusable => false;

        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            var filter = (HasRolesActionFilter)serviceProvider.GetService(typeof(HasRolesActionFilter));

            filter.Roles = Roles;
            filter.RolesBehaviour = RolesBehaviour;

            return filter;
        }
    }
}
