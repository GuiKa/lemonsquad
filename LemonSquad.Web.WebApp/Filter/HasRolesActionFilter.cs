﻿using System.Linq;
using LemSquad.Web.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LemonSquad.Web.WebApp.Filter
{
    public class HasRolesActionFilter : IActionFilter
    {
        private readonly IIdentityContext _identityContext;

        public RolesBehaviour RolesBehaviour { get; set; }
        public string[] Roles { get; set; }

        public HasRolesActionFilter(IIdentityContext identityContext)
        {
            _identityContext = identityContext;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (RolesBehaviour == RolesBehaviour.And)
            {
                if (Roles.Any(x => !_identityContext.Roles.Contains(x)))
                    context.Result = new ForbidResult();
            }
            else
            {
                if (Roles.All(x => !_identityContext.Roles.Contains(x)))
                    context.Result = new ForbidResult();
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // do something after the action executes
        }
    }

    public enum RolesBehaviour
    {
        And=0,
        Or=1
    }
}
