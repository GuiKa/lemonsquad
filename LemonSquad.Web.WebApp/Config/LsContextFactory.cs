﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LemonSquad.Context;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Neovendis.Store.Hmi.WebApp.Config
{
    public class LsContextFactory : IDesignTimeDbContextFactory<LsContext>
    {
        //See: https://wildermuth.com/2018/01/10/Re-thinking-Running-Migrations-and-Seeding-in-ASP-NET-Core-2-0
        public LsContext CreateDbContext(string[] args)
        {
            // Create a configuration 
            var builder = new WebHostBuilder();
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            return new LsContext(
                new DbContextOptionsBuilder<LsContext>()
                .UseSqlServer(config.GetConnectionString("LsContext"))
                    .Options);
        }
    }
}
