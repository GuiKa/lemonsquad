﻿1-Install VS 2017
2-Install Node.js
3-Update the base path in ClientApp/api/api.ts to your IIS uri
4-Update the connection string in appsettings.json
5-Run webpack vendor (see below command)

node node_modules/webpack/bin/webpack.js --config webpack.config.vendor.js
node node_modules/webpack/bin/webpack.js --config webpack.config.js