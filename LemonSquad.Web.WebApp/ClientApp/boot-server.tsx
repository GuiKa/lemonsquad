import * as React from 'react';
import { Provider } from 'react-redux';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { replace } from 'react-router-redux';
import { createMemoryHistory } from 'history';
import { createServerRenderer, RenderResult } from 'aspnet-prerendering';
import { routes } from './routes';
import configureStore from './configureStore';
import { initData } from './boot-common';

const readTokenFromCookies = (cookieData: { key: string, value: string }[]): string => {
    let token = "";
    cookieData.forEach(keyValuePair => {
        if (keyValuePair.key === "access_token")
            token = keyValuePair.value;
    });
    return token;
};

export default createServerRenderer(params => {
    return new Promise<RenderResult>((resolve, reject) => {
        // Prepare Redux store with in-memory history, and dispatch a navigation event
        // corresponding to the incoming URL
        const basename = params.baseUrl.substring(0, params.baseUrl.length - 1); // Remove trailing slash
        const urlAfterBasename = params.url.substring(basename.length);
        const store = configureStore(createMemoryHistory());
        store.dispatch(replace(urlAfterBasename));

        if (params.data.cookies) {
            //Load token value from cookies into the store
            store.dispatch({ type: 'LOAD_TOKEN', token: readTokenFromCookies(params.data.cookies) });
        }

        initData(new Date().getTime(), store.getState, store.dispatch).then(() => {
            // Prepare an instance of the application and perform an inital render that will
            // cause any async tasks (e.g., data access) to begin
            const routerContext: any = {};
            const app = (
                <Provider store={store}>
                    <StaticRouter basename={basename} context={routerContext} location={params.location.path} children={routes} />
                </Provider>
            );
            renderToString(app);

            // If there's a redirection, just send this information back to the host application
            if (routerContext.url) {
                resolve({ redirectUrl: routerContext.url });
                return;
            }

            // Once any async tasks are done, we can perform the final render
            // We also send the redux store state, so the client can continue execution where the server left off
            params.domainTasks.then(() => {
                resolve({
                    html: renderToString(app),
                    globals: {
                        initialReduxState: store.getState()
                    }
                });
            }, reject); // Also propagate any errors back into the host application
        }).catch((err) => {
            reject(err);
        });
    });
});
