﻿import { ApplicationState } from './store';
import * as AccountStore from './store/Account';
import * as SeedStore from './store/Seed';
import { getDefaultHeaders } from './utils/utils';
import configureStore from './configureStore';

//Load data needed at the start of the app, seed and current user
//Only need in server side rendering
export const initData = (requestTime: number, getState: () => ApplicationState,
    dispatch: (action: AccountStore.KnownAction | SeedStore.KnownAction) => void): Promise<any> => {

    let requestUserTask = AccountStore.requestCurrentUser(requestTime, dispatch, getState);
    let requestSeed = SeedStore.requestSeed(requestTime, dispatch, getState);

    return Promise.all([requestUserTask, requestSeed]);
}