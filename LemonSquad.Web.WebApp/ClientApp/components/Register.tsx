﻿import * as React from 'react';
import * as AccountStore from '../store/Account';
import * as Api from '../api/api';
import RegisterForm from "./RegisterForm";
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from "ClientApp/store";
import { connect } from 'react-redux';

type RegisterProps = RegisterOwnProps
    & AccountStore.AccountState
    & typeof AccountStore.actionCreators
    & RouteComponentProps<RegisterParams>;

interface RegisterOwnProps {
}

interface RegisterParams {
}


interface RegisterState {
}

class Register extends React.Component<RegisterProps, RegisterState> {
    constructor(props: RegisterProps) {
        super(props);
    }

    public render() {
        return <div style={{ width: "100%", height: "100%" }}>
            <h2>Register</h2>
            <RegisterForm onSubmit={(values) => this.props.requestRegister(
                values as Api.RegisterModel,
                new Date().getTime())} />
        </div>;
    }
}

export default connect(
    (state: ApplicationState) => state.account,
    AccountStore.actionCreators
)(Register)