import * as Api from '../api/api';
import * as React from 'react';
import { NavLink, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { isAuthenticated } from '../security/UserIsAuthenticated';
import { ApplicationState } from '../store';

interface NavMenuProps {
    isAuthenticated: boolean;
    account: Api.AccountModel;
    roles: Array<Api.RoleModel>;
}

class NavMenu extends React.Component<NavMenuProps, {}> {
    public render() {
        return (
            <div className='main-nav'>
                <div className='navbar navbar-inverse'>
                    <div className="navflex" style={{ display: "flex", height: "100%" }}>
                        <div style={{ order: 0, flex: 1 }}>
                            <div className='navbar-header'>
                                <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                                    <span className='sr-only'>Toggle navigation</span>
                                    <span className='icon-bar'></span>
                                    <span className='icon-bar'></span>
                                    <span className='icon-bar'></span>
                                </button>
                                <Link className='navbar-brand' to={'/'}>Lemon Squad</Link>
                            </div>
                            <div className='clearfix'></div>
                            <div className='navbar-collapse collapse'>
                                <ul className='nav navbar-nav'>
                                    <li>
                                        <NavLink exact to={'/'} activeClassName='active'>
                                            <span className='glyphicon glyphicon-home'></span> Home
                                        </NavLink>
                                    </li>
                                    {this.props.isAuthenticated &&
                                        <li>
                                            <NavLink to={'/maps'} activeClassName='active'>
                                                <span className='glyphicon glyphicon-picture'></span> Maps
                                            </NavLink>
                                        </li>}
                                    {this.props.isAuthenticated &&
                                        <li>
                                            <NavLink to={'/tactics'} activeClassName='active'>
                                                <span className='glyphicon glyphicon-list-alt'></span> All tactics
                                            </NavLink>
                                        </li>}
                                    {this.props.isAuthenticated &&
                                        <li>
                                            <NavLink to={'/myTactics'} activeClassName='active'>
                                                <span className='glyphicon glyphicon-list-alt'></span> My tactics
                                            </NavLink>
                                        </li>}
                                    {this.props.isAuthenticated && this.props.roles.some(x => x.name === "Admin") &&
                                        <li>
                                            <NavLink to={'/accounts'} activeClassName='active'>
                                                <span className='glyphicon glyphicon-user'></span> Accounts
                                            </NavLink>
                                        </li>}
                                    {this.props.isAuthenticated && this.props.roles.some(x => x.name === "Admin") &&
                                        <li>
                                            <NavLink to={'/tokens'} activeClassName='active'>
                                                <span className='glyphicon glyphicon-tag'></span> Tokens
                                            </NavLink>
                                        </li>}
                                    {!this.props.isAuthenticated &&
                                        <li>
                                            <NavLink to={'/login'} activeClassName='active'>
                                                <span className='glyphicon glyphicon-user'></span> Login
                                            </NavLink>
                                        </li>}
                                    {!this.props.isAuthenticated &&
                                        <li>
                                            <NavLink to={'/register'} activeClassName='active'>
                                                <span className='glyphicon glyphicon-edit'></span> Register
                                            </NavLink>
                                        </li>}
                                </ul>
                            </div>
                        </div>
                        <div style={{ order: 0, flex: "0 0 auto" }}>
                            {this.props.isAuthenticated &&
                                <div style={{
                                    color: "white", width: 150, height: 75,
                                    margin: "auto", display: "flex",
                                    flexDirection: "row", alignItems: "center"
                                }}>
                                <div>
                                    <div style={{ textAlign: "center" }}>
                                        {"Logged as: "}<span style={{ fontSize: 20 }}>{this.props.account.username}</span>
                                    </div>
                                    <div>

                                    </div>
                                </div>
                            </div>}
                        </div>
                    </div>
                </div>
            </div>);
    }
}
export default connect((state: ApplicationState) => ({
    isAuthenticated: isAuthenticated(state),
    account: state.account.currentUser.account,
    roles: state.account.currentUser.roles || []
} as NavMenuProps), {}, null, { pure: false })(NavMenu)
