﻿import * as Api from '../api/api';
import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import { RouteComponentProps } from 'react-router-dom';
import * as EditTacticStore from '../store/EditTactic';
import * as _ from 'lodash';
import Image from "./Image";
import { EditTacticForm } from "./TacticForm";

type EditTacticProps = EditTacticOwnProps
    & EditTacticStore.EditTacticState
    & typeof EditTacticStore.actionCreators
    & RouteComponentProps<EditTacticParams>;

interface EditTacticOwnProps {
    maps: { [id: number]: Api.MapModel };
    mapUplinks: { [id: number]: Api.MapUplinkModel };
}

interface EditTacticParams {
    tacticId: string;
}

class EditTactic extends React.Component<EditTacticProps, {}> {

    updateData(props: EditTacticProps) {
        props.requestTactic(new Date().getTime(), parseInt(props.match.params.tacticId));
    }

    componentWillReceiveProps(nextProps: EditTacticProps) {
        if (this.props.match.params !== nextProps.match.params) {
            this.updateData(nextProps);
        }
    }

    componentWillMount() {
        this.updateData(this.props);
    }

    public render() {
        return (
            <div>
                <h2 style={{ textAlign: "center" }}>Edit tactic</h2>
                {this.props.tactic && <div>
                    <EditTacticForm
                        submitText={"Save changes"}
                        onSubmit={(values) => {
                            return this.props.requestEditTactic(new Date().getTime(), values);
                        }}
                        />
                    </div>}
            </div>
            );
    }
}

export default connect((state: ApplicationState) => ({
    ...state.editTactic,
    maps: state.seed.seed.maps,
    mapUplinks: state.seed.seed.mapUplinks
}), EditTacticStore.actionCreators)(EditTactic)