﻿import * as React from 'react';
import { Field, reduxForm, FormProps, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import * as AccountTokensStore from '../store/AccountTokens';
import * as Api from '../api/api';
import { ApplicationState } from 'ClientApp/store';
import * as FormField from './FormField';
import * as _ from 'lodash';

type CreateAccountTokenFormProps = FormProps<{}, {}, {}>
    & FormField.FormProps
    & CreateAccountTokenFormOwnProps;

interface CreateAccountTokenFormOwnProps {
}

const tokenTypeOptions: Array<{ label: string; value: Api.AccountTokenModelTypeEnum }> = [
    { label: "Register", value: "Register" }
];

class CreateAccountTokenForm extends React.Component<CreateAccountTokenFormProps, {}> {

    public render() {
        return (
            <form className="form-horizontal"
                style={{ maxWidth: 500 }}
                onSubmit={(e) => {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    if (typeof event !== 'undefined') event.returnValue = false;
                    return this.props.handleSubmit(e);
                }}>
                <div className="text-danger" style={{ textAlign: "center" }}>
                    {this.props.error && <span>{this.props.error}</span>}
                </div>
                <div className="form-group row">
                    <label className="col-sm-4 control-label ">{"Type:"}</label>
                    <div className="col-sm-8">
                        <Field className="form-control"
                            validate={FormField.required}
                            name="type"
                            component={FormField.getSelectField(tokenTypeOptions)} />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-4 control-label ">{"Value:"}</label>
                    <div className="col-sm-8">
                        <Field className="form-control"
                            validate={FormField.required}
                            name="value"
                            type="text"
                            component={FormField.renderField} />
                    </div>
                </div>
                <div className="form-group row">
                    <div className="col-sm-4"> </div>
                    <div className="col-sm-8">
                        <button
                            style={{ paddingLeft: 30, paddingRight: 30, float: "right" }}
                            type="submit"
                            disabled={this.props.submitting}
                            className="btn btn-lg btn-primary">{"Create"}</button>
                    </div>
                </div>
            </form>
        );
    }
}

let formWrapped = reduxForm({ form: 'createAccountToken' })(CreateAccountTokenForm) as any;
const selector = formValueSelector('createAccountToken')

export default connect((state: ApplicationState) => {
    return {
    } as CreateAccountTokenFormOwnProps;
})(formWrapped) as any
