﻿import * as React from 'react';
import * as AccountStore from '../store/Account';
import * as Api from '../api/api';
import LoginForm from "./LoginForm";
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from "ClientApp/store";
import { connect } from 'react-redux';

type LoginProps = LoginOwnProps
    & AccountStore.AccountState
    & typeof AccountStore.actionCreators
    & RouteComponentProps<LoginParams>;

interface LoginOwnProps {
}

interface LoginParams {
    redirectTo: string;
}


interface LoginState {
}

class Login extends React.Component<LoginProps, LoginState> {
    constructor(props: LoginProps) {
        super(props);
    }

    public render() {
        return <div style={{ width: "100%", height: "100%" }}>
            <h2>Login</h2>
            <LoginForm onSubmit={(values) => this.props.requestLogin(
                values as Api.LoginModel,
                new Date().getTime())} />
        </div>;
    }
}

export default connect(
    (state: ApplicationState) => state.account,
    AccountStore.actionCreators
)(Login)