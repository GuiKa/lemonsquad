﻿import * as React from 'react';
import {
    Field, reduxForm, FormProps,
    formValueSelector, FieldArray,
    FieldsProps, WrappedFieldArrayProps
} from 'redux-form';
import { connect } from 'react-redux';
import * as MyTacticsStore from '../store/MyTactics';
import * as Api from '../api/api';
import { ApplicationState } from '../store';
import * as FormField from './FormField';
import ImageUpload from './ImageUpload';
import * as _ from 'lodash';

type TacticFormProps = FormProps<{}, {}, {}>
    & FormField.FormProps
    & TacticFormOwnProps
    & TacticFormExternalProps;

interface TacticFormOwnProps {
    mapId: number;
    maps: Array<Api.MapModel>;
    mapUplinks: Array<Api.MapUplinkModel>;
}

interface TacticFormExternalProps {
    onSubmit: (values: Api.TacticModel) => void;
    submitText: string;
}

const descriptionField = FormField.textAreaField(5, 45);
const contentField = FormField.textAreaField(5, 45);
const statusOptions: Array<{ label: string; value: Api.TacticModelStatusEnum }> = [
    { label: "Opened (visible)", value: "Opened" },
    { label: "Closed (not visible)", value: "Closed" }
];
const teamOptions: Array<{ label: string; value: Api.TacticModelTeamEnum }> = [
    { label: "Marsoc", value: "Marsoc" },
    { label: "Volk", value: "Volk" }
];

class TacticForm extends React.Component<TacticFormProps, {}> {

    get mapOptions(): Array<{ label: string; value: number }> {
        return this.props.maps.map(x => ({
            label: x.name,
            value: x.mapId
        }));
    }

    get mapUplinkOptions(): Array<{ label: string; value: number }> {
        return this.props.mapUplinks
            .filter(x => x.mapId === this.props.mapId)
            .map(x => ({
                label: x.name,
                value: x.mapUplinkId
            }));
    }

    submitWrapped(e) {
        let maybePromise = (this.props.handleSubmit(e) as any);
        if (maybePromise.then) {
            let promise = maybePromise as Promise<any>;
            if (promise.then) {
                promise
                    .then(() => {
                        this.props.reset();
                    });
            }
        }
    }

    public render() {
        return (
            <form className="form-horizontal"
                style={{ maxWidth: 2000, padding: 10 }}
                onSubmit={(e) => {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    if (typeof event !== 'undefined') event.returnValue = false;
                    return this.submitWrapped(e);
                }}>
                <div className="text-danger" style={{ textAlign: "center" }}>
                    {this.props.error && <span>{this.props.error}</span>}
                </div>
                <fieldset className="form-group container-fluid">
                    <legend>Tactic info</legend>
                    <div className="form-group row">
                        <label className="col-sm-4 control-label ">{"Team:"}</label>
                        <div className="col-sm-8">
                            <Field className="form-control"
                                validate={FormField.required}
                                name="team"
                                component={FormField.getSelectField(teamOptions)} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 control-label ">{"Map:"}</label>
                        <div className="col-sm-8">
                            <Field className="form-control"
                                validate={FormField.required}
                                name="mapId"
                                component={FormField.getSelectField(this.mapOptions)} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 control-label ">{"Uplink:"}</label>
                        <div className="col-sm-8">
                            <Field className="form-control"
                                validate={FormField.required}
                                name="mapUplinkId"
                                component={FormField.getSelectField(this.mapUplinkOptions)} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 control-label ">{"Status:"}</label>
                        <div className="col-sm-8">
                            <Field className="form-control"
                                validate={FormField.required}
                                name="status"
                                component={FormField.getSelectField(statusOptions)}
                                type="text" />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 control-label ">{"Title:"}</label>
                        <div className="col-sm-8">
                            <Field className="form-control"
                                validate={FormField.required}
                                name="title"
                                component={FormField.renderField}
                                type="text" />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 control-label ">{"Description:"}</label>
                        <div className="col-sm-8">
                            <Field className="form-control"
                                validate={[]}
                                name="description"
                                component={descriptionField} />
                        </div>
                    </div>
                    
                </fieldset>
                <fieldset className="form-group">
                    <legend>Tactic contents</legend>
                    <FieldArray
                        name="tacticContents"
                        component={TacticContents} />
                </fieldset>
                <div className="form-group row">
                    <div className="col-sm-4"> </div>
                    <div className="col-sm-8">
                        <button
                            style={{ paddingLeft: 30, paddingRight: 30, float: "right" }}
                            type="submit"
                            disabled={this.props.submitting || this.props.pristine}
                            className="btn btn-lg btn-primary">{this.props.submitText}</button>
                    </div>
                </div>
            </form>
        );
    }
}

type TacticContentsProps = TacticContentsOwnProps
    & WrappedFieldArrayProps<Api.TacticContentModel>;
interface TacticContentsOwnProps {
    tacticId: number
}

const imageField = ({
    input,
    meta: { touched, error, warning },
    key
}) => {
    return <div>
        <div>
            <ImageUpload
                fieldKey={key}
                value={input.value}
                onChange={(imageId) => input.onChange(imageId)}
            />
        </div>
        <div className="text-danger">
            {touched &&
                ((error && <span>{error}</span>) ||
                    (warning && <span>{warning}</span>))}
        </div>
    </div> 
}

class TacticContents extends React.Component<TacticContentsProps, {}> {
    public render() {
        return (
            <div>
                {this.props.fields.map((member, index) =>
                    (<div key={index} style={{
                        marginTop: 20,
                        padding: 20,
                        backgroundColor: "#f2f2f2",
                        borderRadius: 20
                    }}>
                        <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                            <button
                                type="button"
                                style={{
                                    fontSize: 16, paddingBottom: 2,
                                    flex: "0 0 auto", marginRight: 5
                                }}
                                className="btn btn-secondary btn-sm"
                                title="Remove"
                                onClick={() => this.props.fields.remove(index)}>
                                <i className="glyphicon glyphicon-remove"></i>
                            </button>
                            <h4>Tactic content #{index + 1}</h4>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-4 control-label ">{"Title:"}</label>
                            <div className="col-sm-8">
                                <Field className="form-control"
                                    validate={FormField.required}
                                    name={`${member}.title`}
                                    component={FormField.renderField}
                                    type="text" />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-4 control-label ">{"Content:"}</label>
                            <div className="col-sm-8">
                                <Field className="form-control"
                                    validate={[]}
                                    name={`${member}.content`}
                                    component={contentField}
                                    type="text" />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-4 control-label ">{"Image:"}</label>
                            <div className="col-sm-8">
                                <Field
                                    validate={[]}
                                    name={`${member}.imageId`}
                                    component={({
                                        input,
                                        meta: { touched, error, warning }
                                    }) => imageField({
                                        input: input,
                                            meta: { touched, error, warning },
                                            key: "t" + this.props.tacticId + "tc" + index
                                    })}
                                    type="text" />
                            </div>
                        </div>
                    </div>))}
                <div style={{ textAlign: "center", marginTop: 10 }}>
                    <button
                        type="button"
                        className="btn btn-secondary btn-lg"
                        onClick={() => this.props.fields.push({
                            tacticId: this.props.tacticId,
                            title: "Step " + (this.props.fields.length + 1)
                        })}>
                        <i className="glyphicon glyphicon-arrow-down"></i> Add content <i className="glyphicon glyphicon-arrow-down"></i>
                    </button>
                </div>
            </div>
            );
    }
}

let editForm = reduxForm({
    form: 'EditTacticForm',
    destroyOnUnmount: false,
    enableReinitialize: true
})(TacticForm) as any;
const editSelector = formValueSelector('EditTacticForm')

export const EditTacticForm = connect((state: ApplicationState) => {
    const mapId = editSelector(state, 'mapId')
    return {
        maps: _.values(state.seed.seed.maps),
        mapUplinks: _.values(state.seed.seed.mapUplinks),
        mapId: mapId,
        initialValues: state.editTactic.tactic
    } as TacticFormOwnProps;
})(editForm) as React.ComponentClass<TacticFormExternalProps>;

let createForm = reduxForm({
    form: 'CreateTacticForm',
    destroyOnUnmount: false,
    enableReinitialize: true
})(TacticForm) as any;
const createSelector = formValueSelector('CreateTacticForm')

export const CreateTacticForm = connect((state: ApplicationState) => {
    const mapId = createSelector(state, 'mapId')
    return {
        maps: _.values(state.seed.seed.maps),
        mapUplinks: _.values(state.seed.seed.mapUplinks),
        mapId: mapId
    } as TacticFormOwnProps;
})(createForm) as React.ComponentClass<TacticFormExternalProps>;