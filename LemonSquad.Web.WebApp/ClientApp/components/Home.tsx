﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

export default class Home extends React.Component<RouteComponentProps<{}>, {}> {
    public render() {
        return <div>
            <h2>Lemon Squad</h2>
            <p>
                Welcome to the super secret Emma Watson! You have to be logged in to see the super secret content.
            </p>
        </div>;
    }
}
