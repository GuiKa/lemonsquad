﻿import * as Api from '../api/api';
import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import { RouteComponentProps } from 'react-router-dom';
import * as TacticsStore from '../store/Tactics';
import * as _ from 'lodash';
import ReactTable from "react-table";
import Image from "./Image";

type TacticsProps = TacticsOwnProps
    & TacticsStore.TacticsState
    & typeof TacticsStore.actionCreators
    & RouteComponentProps<TacticsParams>;

interface TacticsOwnProps {
    maps: { [id: number]: Api.MapModel };
    mapUplinks: { [id: number]: Api.MapUplinkModel };
}

interface TacticsParams {
}

class Tactics extends React.Component<TacticsProps, {}> {

    updateData(props: TacticsProps) {
        props.requestTactics(new Date().getTime());
    }

    componentWillReceiveProps(nextProps: TacticsProps) {
        if (this.props.match.params !== nextProps.match.params) {
            this.updateData(nextProps);
        }
    }

    componentWillMount() {
        this.updateData(this.props);
    }

    public render() {
        return (
            <div style={{ height: "100%", width: "100%" }}>
                <h2 style={{ textAlign: "center" }}>Tactics</h2>
                <div>
                    <h3>Tactics table</h3>
                    <ReactTable
                        defaultPageSize={10}
                        data={this.props.tactics}
                        columns={[
                            {
                                Header: <div>Team</div>,
                                id: "team",
                                accessor: (d: Api.TacticModel) => d.team
                            },
                            {
                                Header: <div>Map</div>,
                                id: "map",
                                accessor: (d: Api.TacticModel) => this.props.maps[this.props.mapUplinks[d.mapUplinkId].mapId].name
                            },
                            {
                                Header: <div>Uplink</div>,
                                id: "uplink",
                                accessor: (d: Api.TacticModel) => this.props.mapUplinks[d.mapUplinkId].name
                            },
                            {
                                Header: <div>Title</div>,
                                id: "title",
                                accessor: (d: Api.TacticModel) => d.title
                            },
                            {
                                Header: <div>Author</div>,
                                id: "author",
                                accessor: (d: Api.TacticModel) => d.account.username
                            },
                            {
                                Header: <div></div>,
                                id: "actions",
                                accessor: (d: Api.TacticModel) => d.tacticId,
                                Cell: row => <div style={{ color: "blue" }}>
                                    <span
                                        style={{ cursor: "pointer" }}
                                        onClick={(e) => {
                                            this.props.goToTactic(row.value);
                                            e.preventDefault();
                                        }}>
                                        show
                                        </span>
                                </div>
                            }
                        ]}
                    />
                </div>
            </div>
        );
    }
}

export default connect((state: ApplicationState) => ({
    ...state.tactics,
    maps: state.seed.seed.maps,
    mapUplinks: state.seed.seed.mapUplinks
} as TacticsProps), TacticsStore.actionCreators)(Tactics)