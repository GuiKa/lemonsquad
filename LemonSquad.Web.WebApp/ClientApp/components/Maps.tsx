﻿import * as Api from '../api/api';
import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as Map from '../store/Map';
import * as _ from 'lodash';
import Image from "./Image";

type MapsProps = MapsOwnProps
    & typeof Map.actionCreators;

interface MapsOwnProps {
    maps: Array<Api.MapModel>;
}

class Maps extends React.Component<MapsProps, {}> {
    public render() {
        return (
            <div>
                <h2>Maps</h2>
                <div>
                    <div style={{
                        display: "grid",
                        gridTemplateColumns: "1fr 1fr 1fr",
                        gridGap: 5
                    }}>
                        {this.props.maps.map(map =>
                            <div key={map.mapId} style={{ width: "100%", height: 200 }}>
                                <div style={{
                                    cursor: "pointer",
                                    width: "100%", height: "100%",
                                    display: "flex", flexDirection: "column"
                                }}
                                    onClick={(e) => {
                                        this.props.goToMap(map.mapId);
                                        e.preventDefault();
                                    }}
                                >
                                    <div style={{ order: 0, flex: 1 }}>
                                        <Image src={"/api/Image/GetMapImage?thumb=true&code=" + map.code} />
                                    </div>
                                    <div style={{
                                        order: 1, flex: "0 0 auto", textAlign: "center",
                                        fontSize: 20, fontWeight: "bold"
                                    }}>
                                        {map.name}
                                    </div>
                                </div>
                            </div>)}
                    </div>
                </div>
            </div>
            );
    }
}

export default connect((state: ApplicationState) => ({
    maps: _.values(state.seed.seed.maps)
}), Map.actionCreators)(Maps)