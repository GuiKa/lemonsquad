﻿import * as React from 'react';
import * as Moment from 'moment';
import Select, { Option } from './Select';

//Fixes because @types for redux-form has been made by monkeys
export interface FormProps { };

export const required = value => (value ? undefined : "required");
export const requiredOrZero = value => (value || value === 0 ? undefined : "required");

export const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined

export const minLength = min => value =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined

const passwordRegex = /^(?=.*[0-9])(?=.*[A-Za-z])/;
export const passwordValid = value =>
    value && passwordRegex.test(value) ? undefined : "Your password must contains a letter and a number";

const phoneRegex = /^[0-9 ]*/;
export const phoneValid = value => !value || phoneRegex.test(value) ? undefined : "A phone must only contains numbers";

export const renderField = ({
    disabled,
    input,
    className,
    style,
    type,
    meta: { touched, error, warning }
}) => (
        <div>
            <div>
                <input style={style} {...input} className={className} type={type} disabled={disabled} />
            </div>
            <div className="text-danger">
                {touched &&
                    ((error && <span>{error}</span>) ||
                        (warning && <span>{warning}</span>))}
            </div>
        </div>
    )


export const textAreaField = (row: number, cols: number) => ({
    input,
    className,
    type,
    meta: { touched, error, warning }
}) => (
        <div>
            <div>
                <textarea value={input.value}
                    onChange={(e) => { input.onChange(e.target.value) }}
                    rows={row} cols={cols} />
            </div>
            <div className="text-danger">
                {touched &&
                    ((error && <span>{error}</span>) ||
                        (warning && <span>{warning}</span>))}
            </div>
    </div>
);

export const fileField = (field: any) => (
    <input
        type="file"
        onChange={(e) => field.input.onChange(e.target.files[0])}
    />
);

export const getSelectField = (options: Array<Option>) => ({
    input,
    className,
    type,
    meta: { touched, error, warning }
}) => (
        <div>
            <Select
                options={options}
                value={input.value}
                onChange={(value) => input.onChange(value)}
            />
            <div className="text-danger">
                {touched &&
                    ((error && <span>{error}</span>) ||
                        (warning && <span>{warning}</span>))}
            </div>
        </div>
    );