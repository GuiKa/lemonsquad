﻿import * as React from "react";
import * as Api from '../api/api';
import { ApplicationState } from '../store';
import { connect } from 'react-redux';
import Image from './Image';
import * as ImageStore from '../store/Image';

type ImageUploadProps = ImageUploadOwnProps
    & ImageStore.ImageState
    & typeof ImageStore.actionCreators;

interface ImageUploadOwnProps {
    fieldKey: string;
    value: number;
    onChange: (imageId: number) => void;
}

class ImageUpload extends React.Component<ImageUploadProps, {}> {
    fileInput: HTMLInputElement;

    get imageUploadState(): ImageStore.ImageUploadState {
        return this.props.imageStates[this.props.fieldKey]
            || ImageStore.unloadedUploadState;
    }

    public render() {
        return (
            <div>
                <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                    <div style={{ height: 50, width: 50 }}>
                        {this.props.value &&
                            <Image src={"/api/Image/GetImageContent?imageId=" + this.props.value} />}
                        {!this.props.value &&
                        <div>no image</div>}
                    </div>
                    <input
                        type="file"
                        style={{ order: 0, flex: "0 0 auto" }}
                        ref={(input) => this.fileInput = input} />
                    {!this.props.value && <button
                        type="button"
                        className="btn btn-secondary btn-sm"
                        disabled={this.imageUploadState.isLoading}
                        onClick={(e) => {
                            ((this.props.requestCreateImage(new Date().getTime(),
                                this.props.fieldKey,
                                this.fileInput.files.item(0)) as any) as Promise<any>)
                                .then(() => {
                                    this.props.onChange(this.props.imageStates[this.props.fieldKey].imageId);
                                });
                            e.preventDefault();
                            e.stopPropagation();
                        }}
                        style={{ order: 1, flex: "0 0 auto" }}>
                        <i className="glyphicon glyphicon-cloud-upload"></i>
                    </button>}
                    {this.props.value && <i className="glyphicon glyphicon-ok" style={{ color: "green" }}></i>}
                </div>
            </div>
            );
    }
}

export default connect((state: ApplicationState) => state.image,
    ImageStore.actionCreators)(ImageUpload) as any