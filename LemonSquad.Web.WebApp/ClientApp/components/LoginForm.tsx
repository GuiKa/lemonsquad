﻿import * as React from 'react';
import { Field, reduxForm, FormProps } from 'redux-form';
import { connect } from 'react-redux';
import * as AccountStore from '../store/Account';
import * as Api from '../api/api';
import { ApplicationState } from 'ClientApp/store';
import * as FormField from './FormField';

type LoginFormProps = FormProps<{},{},{}> & LoginFormOwnProps;

interface LoginFormOwnProps {
    handleSubmit: (values: any) => void;
    submitting: boolean;
}

class LoginForm extends React.Component<LoginFormProps, {}> {

    public render() {
        return (
            <form className="form-horizontal"
                style={{ maxWidth: 500 }}
                onSubmit={(e) => {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                if (typeof event !== 'undefined') event.returnValue = false;
                return this.props.handleSubmit(e);
            }}>
                <div className="text-danger" style={{ textAlign: "center" }}>
                    {this.props.error && <span>{this.props.error}</span>}
                </div>
                <div className="form-group row">
                    <label className="col-sm-4 control-label ">{"Username:"}</label>
                    <div className="col-sm-8">
                        <Field className="form-control"
                            validate={FormField.required}
                            name="username"
                            component={FormField.renderField}
                            type="text" />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-4 control-label ">{"Password:"}</label>
                    <div className="col-sm-8">
                        <Field className="form-control"
                            validate={FormField.required}
                            name="password"
                            component={FormField.renderField}
                            type="password" />
                    </div>
                </div>
                <div className="form-group row">
                    <div className="col-sm-4"> </div>
                    <div className="col-sm-8">
                        <button
                            style={{ paddingLeft: 30, paddingRight: 30, float: "right" }}
                            type="submit"
                            disabled={this.props.submitting}
                            className="btn btn-lg btn-primary">{"Login"}</button>
                    </div>
                </div>
            </form>
            );
    }
}

export default reduxForm({ form: 'login' })(LoginForm) as any;
