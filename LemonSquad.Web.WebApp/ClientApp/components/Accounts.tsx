﻿import * as Api from '../api/api';
import * as React from 'react';
import * as AccountsStore from '../store/Accounts';
import * as Moment from 'moment';
import { NavLink, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import ReactTable from "react-table";

type AccountsProps = AccountsOwnProps
    & AccountsStore.AccountsState
    & typeof AccountsStore.actionCreators;

interface AccountsOwnProps {
}

class Accounts extends React.Component<AccountsProps, {}> {

    updateData(props: AccountsProps) {
        props.requestAccounts(new Date().getTime());
    }

    componentWillReceiveProps(nextProps: AccountsProps) {
    }

    componentWillMount() {
        this.updateData(this.props);
    }

    public render() {
        return (<div>
            <h2>Accounts</h2>
            <div>
                <h3>Account table</h3>
                <ReactTable
                    defaultPageSize={10}
                    data={this.props.accounts}
                    columns={[
                        {
                            Header: <div>Username</div>,
                            id: "username",
                            accessor: (d: Api.AccountModel) => d.username
                        },
                        {
                            Header: <div>Creation date</div>,
                            id: "creationDate",
                            accessor: (d: Api.AccountModel) => d.creationDate,
                            Cell: row => <div>{Moment(row.value).format("DD/MM/YYYY")}</div>
                        },
                        {
                            Header: <div>Enabled</div>,
                            id: "enabled",
                            accessor: (d: Api.AccountModel) => d.enabled,
                            Cell: row => <div>{row.value ? "yes" : "no"}</div>
                        },
                        {
                            Header: <div></div>,
                            id: "actions",
                            accessor: (d: Api.AccountModel) => d,
                            Cell: row => {
                                let account = row.value as Api.AccountModel;

                                return <div style={{ color: "blue" }}>
                                <span
                                    style={{ cursor: "pointer" }}
                                    onClick={(e) => {
                                        this.props.requestUpdateEnabled(new Date().getTime(), account.accountId, !account.enabled);
                                        e.preventDefault();
                                    }}>
                                    enable/disable
                                </span>
                            </div>
                            }
                        }
                    ]}
                />
            </div>
        </div>);
    }
}

export default connect((state: ApplicationState) => ({
    ...state.accounts
}), AccountsStore.actionCreators)(Accounts)