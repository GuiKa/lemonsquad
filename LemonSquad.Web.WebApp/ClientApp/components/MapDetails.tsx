﻿import * as Api from '../api/api';
import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import { RouteComponentProps } from 'react-router-dom';
import * as MapStore from '../store/Map';
import * as _ from 'lodash';
import Image from "./Image";
import ReactTable from "react-table";

type MapDetailsProps = MapDetailsOwnProps
    & MapStore.MapState
    & typeof MapStore.actionCreators
    & RouteComponentProps<MapDetailsParams>;

interface MapDetailsOwnProps {
    maps: { [id: number]: Api.MapModel };
    mapUplinks: { [id: number]: Api.MapUplinkModel };
}

interface MapDetailsParams {
    mapId: string;
}

class MapDetails extends React.Component<MapDetailsProps, {}> {

    updateData(props: MapDetailsProps) {
        props.updateMapId(parseInt(props.match.params.mapId));
        props.requestTactics(new Date().getTime());
    }

    componentWillReceiveProps(nextProps: MapDetailsProps) {
        if (this.props.match.params !== nextProps.match.params) {
            this.updateData(nextProps);
        }
    }

    componentWillMount() {
        this.updateData(this.props);
    }

    get map(): Api.MapModel {
        return this.props.maps[parseInt(this.props.match.params.mapId || "1")];
    }

    public render() {
        return (
            <div style={{ height: "100%", width: "100%" }}>
                <h2 style={{ textAlign: "center" }}>{this.map.name}</h2>
                <div style={{ textAlign: "center" }}>
                    {this.map.description}
                </div>
                <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                    <div style={{ height: 600, width: 600 }}>
                        <Image fit={"contain"} src={"/api/Image/GetMapImage?code=" + this.map.code} />
                    </div>
                </div>
                <div>
                    <h3 style={{ textAlign: "center" }}>Tactics</h3>
                    <ReactTable
                        data={this.props.tactics}
                        defaultPageSize={10}
                        columns={[
                            {
                                Header: <div>Team</div>,
                                id: "team",
                                accessor: (d: Api.TacticModel) => d.team
                            },
                            {
                                Header: <div>Title</div>,
                                id: "title",
                                accessor: (d: Api.TacticModel) => d.title
                            },
                            {
                                Header: <div>Uplink</div>,
                                id: "uplink",
                                accessor: (d: Api.TacticModel) => this.props.mapUplinks[d.mapUplinkId].name
                            },
                            {
                                Header: <div>Author</div>,
                                id: "author",
                                accessor: (d: Api.TacticModel) => d.account.username
                            },
                            {
                                Header: <div></div>,
                                id: "actions",
                                accessor: (d: Api.TacticModel) => d.tacticId,
                                Cell: row => <div style={{ color: "blue" }}>
                                    <span
                                        style={{ cursor: "pointer" }}
                                        onClick={(e) => {
                                            this.props.goToTactic(row.value);
                                            e.preventDefault();
                                        }}>
                                        show
                                    </span>
                                </div>
                            }
                        ]}
                    />
                </div>
            </div>
        );
    }
}

export default connect((state: ApplicationState) => ({
    ...state.map,
    maps: state.seed.seed.maps,
    mapUplinks: state.seed.seed.mapUplinks
} as MapDetailsProps), MapStore.actionCreators)(MapDetails)