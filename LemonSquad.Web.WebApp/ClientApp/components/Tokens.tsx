﻿import * as Api from '../api/api';
import * as React from 'react';
import * as Moment from 'moment';
import * as AccountTokensStore from '../store/AccountTokens';
import { NavLink, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import ReactTable from "react-table";
import CreateAccountTokenForm from "./CreateAccountTokenForm";

type TokensProps = TokensOwnProps
    & AccountTokensStore.AccountTokensState
    & typeof AccountTokensStore.actionCreators;

interface TokensOwnProps {
}

class Tokens extends React.Component<TokensProps, {}> {

    updateData(props: TokensProps) {
        props.requestAccountTokens(new Date().getTime());
    }

    componentWillReceiveProps(nextProps: TokensProps) {
    }

    componentWillMount() {
        this.updateData(this.props);
    }

    public render() {
        return (<div>
            <h2>Tokens</h2>
            <div>
                <h3>Creation form</h3>
                <CreateAccountTokenForm
                    onSubmit={(values) => {
                        return this.props.requestCreateAccountToken(new Date().getTime(), values);
                    }}
                />
            </div>
            <div>
                <h3>Token table</h3>
                <ReactTable
                    defaultPageSize={10}
                    data={this.props.accountTokens}
                    columns={[
                        {
                            Header: <div>Type</div>,
                            id: "type",
                            accessor: (d: Api.AccountTokenModel) => d.type
                        },
                        {
                            Header: <div>Value</div>,
                            id: "value",
                            accessor: (d: Api.AccountTokenModel) => d.value
                        },
                        {
                            Header: <div>Used by</div>,
                            id: "account",
                            accessor: (d: Api.AccountTokenModel) => d.account ? d.account.username : ""
                        },
                        {
                            Header: <div>Creation date</div>,
                            id: "creationDate",
                            accessor: (d: Api.AccountTokenModel) => d.creationDate,
                            Cell: row => <div>{Moment(row.value).format("DD/MM/YYYY")}</div>
                        },
                        {
                            Header: <div></div>,
                            id: "actions",
                            accessor: (d: Api.AccountTokenModel) => d,
                            Cell: row => {
                                let token = row.value as Api.AccountTokenModel;
                                return <div style={{ color: "blue" }}>
                                    <span
                                        style={{ cursor: "pointer" }}
                                        onClick={(e) => {
                                            this.props.requestDeleteAccountToken(new Date().getTime(), token.accountTokenId);
                                            e.preventDefault();
                                        }}>
                                        delete
                                    </span>
                                </div>
                            }
                        }
                    ]}
                />
            </div>
        </div>);
    }
}

export default connect((state: ApplicationState) => ({
    ...state.accountTokens
}), AccountTokensStore.actionCreators)(Tokens)