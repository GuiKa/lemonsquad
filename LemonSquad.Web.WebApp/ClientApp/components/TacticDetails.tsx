﻿import * as Api from '../api/api';
import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import { RouteComponentProps, Link } from 'react-router-dom';
import * as TacticStore from '../store/Tactic';
import * as _ from 'lodash';
import Image from "./Image";

type TacticDetailsProps = TacticDetailsOwnProps
    & TacticStore.TacticState
    & typeof TacticStore.actionCreators
    & RouteComponentProps<TacticDetailsParams>;

interface TacticDetailsOwnProps {
    maps: { [id: number]: Api.MapModel };
    mapUplinks: { [id: number]: Api.MapUplinkModel };
}

interface TacticDetailsParams {
    tacticId: string;
}

class TacticDetails extends React.Component<TacticDetailsProps, {}> {

    updateData(props: TacticDetailsProps) {
        props.requestTactic(new Date().getTime(), parseInt(props.match.params.tacticId));
    }

    componentWillReceiveProps(nextProps: TacticDetailsProps) {
        if (this.props.match.params !== nextProps.match.params) {
            this.updateData(nextProps);
        }
    }

    componentWillMount() {
        this.updateData(this.props);
    }

    public render() {
        return (<div>
            <div style={{ padding: 10 }}>
                <Link to={"/map/" + (this.props.tactic
                    ? this.props.mapUplinks[this.props.tactic.mapUplinkId].mapId 
                    : 1)}>
                    <button className="btn btn-primary">
                        <i className="glyphicon glyphicon-arrow-left"></i> Back to map
                    </button>
                </Link>
            </div>
            {this.props.tactic && <div style={{ padding: 10 }}>
                <h2 style={{ textAlign: "center" }}>{this.props.tactic.title}</h2>
                <div className="form-horizontal" style={{ marginTop: 10 }}>
                    <div className="form-group row">
                        <label className="control-label col-md-4">Team:</label>
                        <div className="col-md-8 form-control-static">
                            {this.props.tactic.team}
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="control-label col-md-4">Map:</label>
                        <div className="col-md-8 form-control-static">
                            {this.props.maps[this.props.mapUplinks[this.props.tactic.mapUplinkId].mapId].name}
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="control-label col-md-4">Uplink:</label>
                        <div className="col-md-8 form-control-static">
                            {this.props.mapUplinks[this.props.tactic.mapUplinkId].name}
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="control-label col-md-4">Description:</label>
                        <div className="col-md-8 form-control-static">
                            <div style={{ whiteSpace: "pre-wrap" }}
                                className="form-control-static">{this.props.tactic.description}</div>
                        </div>
                    </div>
                    <div style={{ marginTop: 10 }}>
                        {this.props.tactic.tacticContents.map(tacticContent => <div key={tacticContent.tacticContentId}
                            style={{ marginBottom: 10 }}>
                            <h3>{tacticContent.title}</h3>
                            <div style={{ whiteSpace: "pre-wrap" }}>
                                {tacticContent.content}
                            </div>
                            <div style={{ height: 600 }}>
                                <Image
                                    fit="contain"
                                    src={"/api/Image/GetImageContent?imageId=" + tacticContent.imageId} />
                            </div>
                        </div>)}
                    </div>
                </div>
            </div>}
        </div>);
    }
}


export default connect((state: ApplicationState) => ({
    ...state.tactic,
    maps: state.seed.seed.maps,
    mapUplinks: state.seed.seed.mapUplinks
} as TacticDetailsProps), TacticStore.actionCreators)(TacticDetails);