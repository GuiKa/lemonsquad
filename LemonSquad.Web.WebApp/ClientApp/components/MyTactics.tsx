﻿import * as Api from '../api/api';
import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import { RouteComponentProps } from 'react-router-dom';
import * as MyTacticsStore from '../store/MyTactics';
import * as _ from 'lodash';
import ReactTable from "react-table";
import Image from "./Image";
import { CreateTacticForm } from "./TacticForm";

type MyTacticsProps = MyTacticsOwnProps
    & MyTacticsStore.MyTacticsState
    & typeof MyTacticsStore.actionCreators
    & RouteComponentProps<MyTacticsParams>;

interface MyTacticsOwnProps {
    maps: { [id: number]: Api.MapModel };
    mapUplinks: { [id: number]: Api.MapUplinkModel };
}

interface MyTacticsParams {
}

class MyTactics extends React.Component<MyTacticsProps, {}> {

    updateData(props: MyTacticsProps) {
        props.requestTactics(new Date().getTime());
    }

    componentWillReceiveProps(nextProps: MyTacticsProps) {
        if (this.props.match.params !== nextProps.match.params) {
            this.updateData(nextProps);
        }
    }

    componentWillMount() {
        this.updateData(this.props);
    }

    public render() {
        return (
            <div style={{ height: "100%", width: "100%" }}>
                <h2 style={{ textAlign: "center" }}>My tactics</h2>
                <div>
                    <h3>Create new</h3>
                    <CreateTacticForm
                        submitText={"Create new tactic"}
                        onSubmit={(values) => {
                            return this.props.requestCreateTactic(new Date().getTime(), values);
                        }}
                    />
                    <div>{"You can set the content later."}</div>
                </div>
                <div>
                    <h3>My tactics table</h3>
                    <ReactTable
                        defaultPageSize={10}
                        data={this.props.tactics}
                        columns={[
                            {
                                Header: <div>Status</div>,
                                id: "status",
                                accessor: (d: Api.TacticModel) => d.status
                            },
                            {
                                Header: <div>Team</div>,
                                id: "team",
                                accessor: (d: Api.TacticModel) => d.team
                            },
                            {
                                Header: <div>Map</div>,
                                id: "map",
                                accessor: (d: Api.TacticModel) => this.props.maps[this.props.mapUplinks[d.mapUplinkId].mapId].name
                            },
                            {
                                Header: <div>Uplink</div>,
                                id: "uplink",
                                accessor: (d: Api.TacticModel) => this.props.mapUplinks[d.mapUplinkId].name
                            },
                            {
                                Header: <div>Title</div>,
                                id: "title",
                                accessor: (d: Api.TacticModel) => d.title
                            },
                            {
                                Header: <div>Author</div>,
                                id: "author",
                                accessor: (d: Api.TacticModel) => d.account.username
                            },
                            {
                                Header: <div></div>,
                                id: "actions",
                                accessor: (d: Api.TacticModel) => d.tacticId,
                                Cell: row => <div style={{ color: "blue" }}>
                                    <span
                                        style={{ cursor: "pointer" }}
                                        onClick={(e) => {
                                            this.props.goToEditTactic(row.value);
                                            e.preventDefault();
                                        }}>
                                        edit
                                        </span>
                                    <span
                                        style={{ cursor: "pointer", marginLeft: 10 }}
                                        onClick={(e) => {
                                            if (confirm("are you sure?")) {
                                                this.props.requestDeleteTactic(new Date().getTime(), row.value);
                                            }
                                            e.preventDefault();
                                        }}>
                                        delete
                                        </span>
                                </div>
                            }
                        ]}
                    />
                </div>
            </div>
        );
    }
}

export default connect((state: ApplicationState) => ({
    ...state.myTactics,
    maps: state.seed.seed.maps,
    mapUplinks: state.seed.seed.mapUplinks
} as MyTacticsProps), MyTacticsStore.actionCreators)(MyTactics)