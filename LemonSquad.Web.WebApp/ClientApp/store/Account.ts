﻿import { fetch, addTask } from "domain-task";
import { Action, Reducer } from 'redux';
import * as Api from "../api/api";
import * as Moment from "moment";
import { getDefaultHeaders } from "../utils/utils";
import { ApplicationState, AppThunkAction } from ".";
import { SubmissionError } from 'redux-form';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface AccountState {
    isLoading: boolean;
    isLoaded: boolean;
    requestTime: number;
    token: string;
    currentUser: Api.CurrentUserModel;
    loginState: LoginState;
    registerState: RegisterState;
}

interface LoginState {
    isLoading: boolean;
    requestTime: number;
}

interface RegisterState {
    isLoading: boolean;
    requestTime: number;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

interface LoadToken { type: "LOAD_TOKEN", token: string }
interface RequestCurrentUser { type: "REQUEST_CURRENT_USER", requestTime: number }
export interface ReceiveCurrentUser { type: "RECEIVE_CURRENT_USER", requestTime: number, currentUser: Api.CurrentUserModel }

interface RequestRegister { type: "REQUEST_REGISTER", model: Api.RegisterModel, requestTime: number }
interface ReceiveRegister { type: "RECEIVE_REGISTER", token: Api.TokenModel, requestTime: number }

interface RequestLogin { type: "REQUEST_LOGIN", login: Api.LoginModel, requestTime: number }
interface ReceiveLogin { type: "RECEIVE_LOGIN", token: Api.TokenModel, requestTime: number }
export interface Logout { type: "LOGOUT" }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownAction = LoadToken | RequestCurrentUser | ReceiveCurrentUser
    | RequestLogin | ReceiveLogin | Logout
    | RequestRegister | ReceiveRegister;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const requestCurrentUser = (requestTime: number, dispatch: (action: KnownAction) => void, getState: () => ApplicationState): Promise<any> => {
    // Only load data if it's something we don't already have (and are not already loading)
    if (getState().account.requestTime === requestTime)
        return Promise.resolve();

    let api = new Api.AccountApi();
    let fetchTask = api.apiAccountGetCurrentUserGet({ credentials: "same-origin", headers: getDefaultHeaders(getState()) })
        .then(data => {
            dispatch({ type: "RECEIVE_CURRENT_USER", requestTime: requestTime, currentUser: data });
        }).catch(error => {
            dispatch({ type: "RECEIVE_CURRENT_USER", requestTime: requestTime, currentUser: {} });
            console.log("Error fetching current client: " + error.message);
        });

    addTask(fetchTask);
    dispatch({ type: "REQUEST_CURRENT_USER", requestTime: requestTime });
    return fetchTask;
};

export const actionCreators = {
    requestCurrentUser: (requestTime: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        return requestCurrentUser(requestTime, dispatch, getState);
    },
    requestLogin: (login: Api.LoginModel, requestTime: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (getState().account.loginState.requestTime === requestTime)
            return Promise.reject("already did");

        let api = new Api.AccountApi();
        let fetchTask = api.apiAccountCreateTokenPost({
            model: login
        }, { credentials: "same-origin" }).then(tokenResult => {
            if (tokenResult.token == null) {
                dispatch({ type: "RECEIVE_LOGIN", token: null, requestTime: requestTime });
                throw new SubmissionError({ _error: tokenResult.message } as any);
            } else {
                dispatch({ type: "RECEIVE_LOGIN", token: tokenResult.token, requestTime: requestTime });
                return requestCurrentUser(new Date().getTime(), dispatch, getState)
                    .then(() => {

                    })
                    .catch(error => {
                        throw error;
                    });
            }
        })
            .catch(error => {
                throw error;
            });
        dispatch({ type: "REQUEST_LOGIN", login: login, requestTime: requestTime });
        addTask(fetchTask);
        return fetchTask;
    },
    requestRegister: (model: Api.RegisterModel, requestTime: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (getState().account.loginState.requestTime === requestTime)
            return Promise.reject("already did");

        let api = new Api.AccountApi();
        let fetchTask = api.apiAccountRegisterPost({
            model: model
        }, { credentials: "same-origin" }).then(tokenResult => {
            if (tokenResult.token == null) {
                dispatch({ type: "RECEIVE_REGISTER", token: null, requestTime: requestTime });
                throw new SubmissionError({ _error: tokenResult.message } as any);
            } else {
                dispatch({ type: "RECEIVE_REGISTER", token: tokenResult.token, requestTime: requestTime });
                return requestCurrentUser(new Date().getTime(), dispatch, getState)
                    .then(() => {

                    })
                    .catch(error => {
                        throw error;
                    });
            }
        })
        .catch(error => {
            throw error;
        });
        dispatch({ type: "REQUEST_REGISTER", model: model, requestTime: requestTime });
        addTask(fetchTask);
        return fetchTask;
    },
};

const unloadedState: AccountState = {
    isLoading: false,
    requestTime: 0,
    token: null,
    isLoaded: false,
    currentUser: {},
    loginState: {
        isLoading: false,
        requestTime: 0
    },
    registerState: {
        isLoading: false,
        requestTime: 0
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<AccountState> = (state: AccountState, incomingAction: Action) => {
    let action = incomingAction as KnownAction;
    switch (action.type) {
        case 'LOAD_TOKEN':
            return {
                ...state,
                token: action.token
            };
        case "REQUEST_CURRENT_USER":
            return {
                ...state,
                isLoading: true,
                isLoaded: false,
                requestTime: action.requestTime
            };
        case "RECEIVE_CURRENT_USER":
            if (action.requestTime === state.requestTime) {
                return {
                    ...state,
                    isLoading: false,
                    isLoaded: true,
                    currentUser: action.currentUser
                };
            }
            break;
        case "REQUEST_LOGIN":
            return {
                ...state,
                loginState: {
                    ...state.loginState,
                    isLoading: true,
                    requestTime: action.requestTime
                }
            }
        case "RECEIVE_LOGIN":
            if (action.requestTime !== state.loginState.requestTime)
                return state;

            if (action.token) {
                return {
                    ...state,
                    currentUser: {
                        expirationDate: action.token.expirationDate
                    },
                    token: action.token.token,
                    loginState: {
                        ...state.loginState,
                        isLoading: false
                    }
                }
            }
            return {
                ...state,
                currentUser: {},
                loginState: {
                    ...state.loginState,
                    isLoading: false
                }
            };
        case "REQUEST_REGISTER":
            return {
                ...state,
                registerState: {
                    ...state.registerState,
                    isLoading: true,
                    requestTime: action.requestTime
                }
            }
        case "RECEIVE_REGISTER":
            if (action.requestTime !== state.registerState.requestTime)
                return state;

            if (action.token) {
                return {
                    ...state,
                    currentUser: {
                        expirationDate: action.token.expirationDate
                    },
                    token: action.token.token,
                    registerState: {
                        ...state.registerState,
                        isLoading: false
                    }
                }
            }
            return {
                ...state,
                currentUser: {},
                registerState: {
                    ...state.registerState,
                    isLoading: false
                }
            };
        case "LOGOUT":
            return {
                ...state,
                currentUser: {},
                token: null
            };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || unloadedState;
};
