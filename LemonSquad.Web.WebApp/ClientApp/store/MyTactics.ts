﻿import { fetch, addTask } from "domain-task";
import { Action, Reducer } from 'redux';
import * as Api from "../api/api";
import * as Moment from "moment";
import { getDefaultHeaders } from "../utils/utils";
import { ApplicationState, AppThunkAction } from ".";
import { push, RouterAction } from "react-router-redux";
import { SubmissionError } from "redux-form";
import { ReceiveEditTactic } from "./EditTactic";

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface MyTacticsState {
    isLoading: boolean;
    requestTime: number;
    tactics: Array<Api.TacticModel>;
    tacticStates: { [id: number]: TacticState };
    createTacticState: CreateTacticState;
}

export interface TacticState {
    isLoading: boolean;
    requestTime: number;
}

interface CreateTacticState {
    isLoading: boolean;
    requestTime: number;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

interface RequestMyTactics { type: "REQUEST_MY_TACTICS"; requestTime: number }
interface ReceiveMyTactics { type: "RECEIVE_MY_TACTICS"; requestTime: number; tactics: Array<Api.TacticModel> }

interface RequestCreateTactic { type: "REQUEST_CREATE_TACTIC"; requestTime: number }
interface ReceiveCreateTactic { type: "RECEIVE_CREATE_TACTIC"; requestTime: number; tactic: Api.TacticModel }

interface RequestDeleteTactic { type: "REQUEST_DELETE_TACTIC"; requestTime: number; tacticId: number }
interface ReceiveDeleteTactic { type: "RECEIVE_DELETE_TACTIC"; requestTime: number; tacticId: number; success: boolean }


// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownAction = RequestMyTactics | ReceiveMyTactics
    | RequestCreateTactic | ReceiveCreateTactic
    | RequestDeleteTactic | ReceiveDeleteTactic
    | ReceiveEditTactic;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestTactics: (requestTime: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (requestTime === getState().myTactics.requestTime)
            return;

        let api = new Api.TacticApi();
        let task = api.apiTacticGetMyTacticsGet({ credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(tactics => {
                dispatch({ type: "RECEIVE_MY_TACTICS", requestTime: requestTime, tactics: tactics });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "RECEIVE_MY_TACTICS", requestTime: requestTime, tactics: [] });
            });

        addTask(task);
        dispatch({ type: "REQUEST_MY_TACTICS", requestTime: requestTime });
    },
    requestDeleteTactic: (requestTime: number, tacticId: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (getState().myTactics.tacticStates[tacticId]
            && requestTime === getState().myTactics.tacticStates[tacticId].requestTime)
            return;

        let api = new Api.TacticApi();
        let task = api.apiTacticDeleteGet({ id: tacticId },
            { credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(tactics => {
                dispatch({ type: "RECEIVE_DELETE_TACTIC", requestTime: requestTime, tacticId: tacticId, success: true });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "RECEIVE_DELETE_TACTIC", requestTime: requestTime, tacticId: tacticId, success: false });
            });

        addTask(task);
        dispatch({ type: "REQUEST_DELETE_TACTIC", requestTime: requestTime, tacticId: tacticId });
    },
    requestCreateTactic: (requestTime: number, tactic: Api.TacticModel): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (requestTime === getState().myTactics.createTacticState.requestTime)
            return Promise.reject("already did");

        let api = new Api.TacticApi();
        let task = api.apiTacticCreatePost({
            model: tactic
        }, { credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(tactic => {
                dispatch({ type: "RECEIVE_CREATE_TACTIC", requestTime: requestTime, tactic: tactic });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "RECEIVE_CREATE_TACTIC", requestTime: requestTime, tactic: null });
                throw new SubmissionError({ _error: "Failed to create tactic"} );
            });

        addTask(task);
        dispatch({ type: "REQUEST_CREATE_TACTIC", requestTime: requestTime });
        //Return promise for redux form
        return task;
    },
    goToEditTactic: (tacticId: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch(push('/editTactic/' + tacticId) as any);
    }
};

const unloadedState: MyTacticsState = {
    isLoading: false,
    requestTime: 0,
    tactics: [],
    tacticStates: {},
    createTacticState: {
        isLoading: false,
        requestTime: 0
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<MyTacticsState> = (state: MyTacticsState, incomingAction: Action) => {
    let action = incomingAction as KnownAction;
    switch (action.type) {
        case "REQUEST_MY_TACTICS":
            return {
                ...state,
                isLoading: true,
                requestTime: action.requestTime
            };
        case "RECEIVE_MY_TACTICS":
            if (state.requestTime !== action.requestTime)
                return state;

            return {
                ...state,
                isLoading: false,
                tactics: action.tactics
            };
        case "REQUEST_CREATE_TACTIC":
            return {
                ...state,
                createTacticState: {
                    ...state.createTacticState,
                    isLoading: true,
                    requestTime: action.requestTime
                }
            };
        case "RECEIVE_CREATE_TACTIC":
            return {
                ...state,
                tactics: state.tactics.concat([action.tactic].filter(x => x)),
                createTacticState: {
                    ...state.createTacticState,
                    //Last one unload
                    isLoading: action.requestTime === state.createTacticState.requestTime
                        ? false
                        : state.createTacticState.isLoading
                }
            };
        case "REQUEST_DELETE_TACTIC":
            return {
                ...state,
                tacticStates: {
                    ...state.tacticStates,
                    [action.tacticId]: {
                        ...state.tacticStates[action.tacticId],
                        isLoading: true,
                        requestTime: action.requestTime
                    }
                }
            };
        case "RECEIVE_DELETE_TACTIC":
            if (state.tacticStates[action.tacticId]
                && state.tacticStates[action.tacticId].requestTime !== action.requestTime)
                return state;

            return {
                ...state,
                tactics: state.tactics.filter(x => x.tacticId !== (action as ReceiveDeleteTactic).tacticId
                    || !(action as ReceiveDeleteTactic).success),
                tacticStates: {
                    ...state.tacticStates,
                    [action.tacticId]: {
                        ...state.tacticStates[action.tacticId],
                        isLoading: false
                    }
                }
            };
        case "RECEIVE_EDIT_TACTIC":
            return {
                ...state,
                tactics: state.tactics
                    .map(x => x.tacticId === (action as ReceiveEditTactic).tactic.tacticId
                        ? {
                            ...(action as ReceiveEditTactic).tactic,
                            account: x.account,
                        }
                        : x)
            };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || unloadedState;
};
