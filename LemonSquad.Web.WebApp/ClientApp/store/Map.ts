﻿import { fetch, addTask } from "domain-task";
import { Action, Reducer } from 'redux';
import * as Api from "../api/api";
import * as Moment from "moment";
import { getDefaultHeaders } from "../utils/utils";
import { ApplicationState, AppThunkAction } from ".";
import { push, RouterAction } from "react-router-redux";

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface MapState {
    isLoading: boolean;
    requestTime: number;
    mapId: number;
    tactics: Array<Api.TacticModel>;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

interface MapGoToMap { type: "MAP_GO_TO_MAP"; mapId: number }
interface MapUpdateMapId { type: "MAP_UPDATE_MAP_ID"; mapId: number }

interface RequestTactics { type: "REQUEST_TACTICS"; requestTime: number }
interface ReceiveTactics { type: "RECEIVE_TACTICS"; requestTime: number; tactics: Array<Api.TacticModel> }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownAction = MapGoToMap | RequestTactics | ReceiveTactics
    | MapUpdateMapId;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    goToMap: (mapId: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch(push("/map/" + mapId) as any);
    },
    goToTactic: (tacticid: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch(push("/tactic/" + tacticid) as any);
    },
    updateMapId: (mapId: number) => <MapUpdateMapId>{ type: "MAP_UPDATE_MAP_ID", mapId: mapId },
    requestTactics: (requestTime: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (requestTime === getState().map.requestTime)
            return;

        let api = new Api.TacticApi();
        let task = api.apiTacticGetMapTacticsGet({
            mapId: getState().map.mapId
        }, { credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(tactics => {
                dispatch({ type: "RECEIVE_TACTICS", requestTime: requestTime, tactics: tactics });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "RECEIVE_TACTICS", requestTime: requestTime, tactics: [] });
            });

        addTask(task);
        dispatch({ type: "REQUEST_TACTICS", requestTime: requestTime });
    }
};

const unloadedState: MapState = {
    isLoading: false,
    requestTime: 0,
    mapId: 0,
    tactics: []
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<MapState> = (state: MapState, incomingAction: Action) => {
    let action = incomingAction as KnownAction;
    switch (action.type) {
        case "MAP_GO_TO_MAP":
            return state;
        case "MAP_UPDATE_MAP_ID":
            return {
                ...state,
                mapId: action.mapId
            };
        case "REQUEST_TACTICS":
            return {
                ...state,
                isLoading: true,
                requestTime: action.requestTime
            };
        case "RECEIVE_TACTICS":
            if (state.requestTime !== action.requestTime)
                return state;

            return {
                ...state,
                isLoading: false,
                tactics: action.tactics
            };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || unloadedState;
};
