import * as Account from './Account';
import * as Seed from './Seed';
import * as Map from './Map';
import * as MyTactics from './MyTactics';
import * as EditTactic from './EditTactic';
import * as Image from './Image';
import * as Tactic from './Tactic';
import * as Accounts from './Accounts';
import * as AccountTokens from './AccountTokens';
import * as Tactics from './Tactics';
import { reducer as reduxForm } from 'redux-form';

// The top-level state object
export interface ApplicationState {
    account: Account.AccountState;
    seed: Seed.SeedState;
    map: Map.MapState;
    myTactics: MyTactics.MyTacticsState;
    editTactic: EditTactic.EditTacticState;
    image: Image.ImageState;
    tactic: Tactic.TacticState;
    tactics: Tactics.TacticsState;
    accounts: Accounts.AccountsState;
    accountTokens: AccountTokens.AccountTokensState;
    form: typeof reduxForm;
}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
    account: Account.reducer,
    seed: Seed.reducer,
    map: Map.reducer,
    myTactics: MyTactics.reducer,
    editTactic: EditTactic.reducer,
    image: Image.reducer,
    tactic: Tactic.reducer,
    tactics: Tactics.reducer,
    accounts: Accounts.reducer,
    accountTokens: AccountTokens.reducer,
    form: reduxForm
};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
