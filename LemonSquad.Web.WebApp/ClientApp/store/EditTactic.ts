﻿import { fetch, addTask } from "domain-task";
import { Action, Reducer } from 'redux';
import * as Api from "../api/api";
import * as Moment from "moment";
import { getDefaultHeaders } from "../utils/utils";
import { ApplicationState, AppThunkAction } from ".";
import { push, RouterAction } from "react-router-redux";
import { SubmissionError } from "redux-form";

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface EditTacticState {
    isLoading: boolean;
    requestTime: number;
    tactic: Api.TacticModel;
    editState: EditState;
    createTacticContentState: CreateTacticContentState;
    tacticContentStates: { [id: number]: TacticContentState };
}

interface EditState {
    isLoading: boolean;
    requestTime: number;
}

interface CreateTacticContentState {
    isLoading: boolean;
    requestTime: number;
}

interface TacticContentState {
    isLoading: boolean;
    requestTime: number;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

interface EditRequestTactic { type: "EDIT_REQUEST_TACTIC"; requestTime: number }
interface EditReceiveTactic { type: "EDIT_RECEIVE_TACTIC"; requestTime: number; tactic: Api.TacticModel }

interface RequestEditTactic { type: "REQUEST_EDIT_TACTIC"; requestTime: number; tactic: Api.TacticModel }
export interface ReceiveEditTactic { type: "RECEIVE_EDIT_TACTIC"; requestTime: number; tactic: Api.TacticModel }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownAction = EditRequestTactic | EditReceiveTactic
    | RequestEditTactic | ReceiveEditTactic;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestTactic: (requestTime: number, tacticId: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (requestTime === getState().editTactic.requestTime)
            return;

        let inStore = getState().myTactics.tactics.find(x => x.tacticId === tacticId);
        if (inStore) {
            dispatch({ type: "EDIT_REQUEST_TACTIC", requestTime: requestTime });
            dispatch({ type: "EDIT_RECEIVE_TACTIC", requestTime: requestTime, tactic: inStore });
            return;
        }

        let api = new Api.TacticApi();
        let task = api.apiTacticGetTacticByIdGet({
            id: tacticId
        }, { credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(tactic => {
                dispatch({ type: "EDIT_RECEIVE_TACTIC", requestTime: requestTime, tactic: tactic });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "EDIT_RECEIVE_TACTIC", requestTime: requestTime, tactic: null });
            });

        addTask(task);
        dispatch({ type: "EDIT_REQUEST_TACTIC", requestTime: requestTime });
    },
    requestEditTactic: (requestTime: number, tactic: Api.TacticModel): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (requestTime === getState().editTactic.editState.requestTime)
            return Promise.reject("already did");

        let api = new Api.TacticApi();
        let task = api.apiTacticEditPost({
            model: tactic
        }, { credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(tacticResult => {
                dispatch({ type: "RECEIVE_EDIT_TACTIC", requestTime: requestTime, tactic: tacticResult });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "RECEIVE_EDIT_TACTIC", requestTime: requestTime, tactic: tactic });
                throw new SubmissionError({ _error: "Failed to edit tactic"} );
            });

        addTask(task);
        dispatch({ type: "REQUEST_EDIT_TACTIC", requestTime: requestTime, tactic: tactic });
        return task;
    }
};

const unloadedState: EditTacticState = {
    isLoading: false,
    requestTime: 0,
    tactic: null,
    editState: {
        isLoading: false,
        requestTime: 0
    },
    createTacticContentState: {
        isLoading: false,
        requestTime: 0
    },
    tacticContentStates: {}
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<EditTacticState> = (state: EditTacticState, incomingAction: Action) => {
    let action = incomingAction as KnownAction;
    switch (action.type) {
        case "EDIT_REQUEST_TACTIC":
            return {
                ...state,
                isLoading: true,
                requestTime: action.requestTime
            };
        case "EDIT_RECEIVE_TACTIC":
            if (state.requestTime !== action.requestTime)
                return state;

            return {
                ...state,
                isLoading: false,
                tactic: action.tactic
            };
        case "REQUEST_EDIT_TACTIC":
            return {
                ...state,
                editState: {
                    ...state.editState,
                    isLoading: true,
                    requestTime: action.requestTime
                }
            };
        case "RECEIVE_EDIT_TACTIC":
            if (state.editState.requestTime !== action.requestTime)
                return state;

            return {
                ...state,
                tactic: {
                    ...action.tactic,
                    //I'd rather not touch this, the server won't anyway
                    account: state.tactic.account
                },
                editState: {
                    ...state.editState,
                    isLoading: false
                }
            };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || unloadedState;
};
