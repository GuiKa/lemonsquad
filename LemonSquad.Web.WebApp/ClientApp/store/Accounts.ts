﻿import { fetch, addTask } from "domain-task";
import { Action, Reducer } from 'redux';
import * as Api from "../api/api";
import * as Moment from "moment";
import { getDefaultHeaders } from "../utils/utils";
import { ApplicationState, AppThunkAction } from ".";
import { push, RouterAction } from "react-router-redux";
import { SubmissionError } from "redux-form";
import { ReceiveEditTactic } from "./EditTactic";

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface AccountsState {
    isLoading: boolean;
    requestTime: number;
    accounts: Array<Api.AccountModel>;
    accountStates: { [id: number]: AccountEditState };
}

interface AccountEditState {
    isLoading: boolean;
    requestTime: number;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

interface RequestAccounts { type: "REQUEST_ACCOUNTS"; requestTime: number }
interface ReceiveAccounts { type: "RECEIVE_ACCOUNTS"; requestTime: number; accounts: Array<Api.AccountModel> }

interface RequestUpdateAccountEnabled { type: "REQUEST_UPDATE_ACCOUNT_ENABLED"; requestTime: number; accountId: number; value: boolean }
interface ReceiveUpdateAccountEnabled { type: "RECEIVE_UPDATE_ACCOUNT_ENABLED"; requestTime: number; accountId: number; value: boolean }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownAction = RequestAccounts | ReceiveAccounts
    | RequestUpdateAccountEnabled | ReceiveUpdateAccountEnabled;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestAccounts: (requestTime: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (requestTime === getState().accounts.requestTime)
            return;

        let api = new Api.AccountApi();
        let task = api.apiAccountGetAccountsGet({ credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(accounts => {
                dispatch({ type: "RECEIVE_ACCOUNTS", requestTime: requestTime, accounts: accounts });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "RECEIVE_ACCOUNTS", requestTime: requestTime, accounts: [] });
            });

        addTask(task);
        dispatch({ type: "REQUEST_ACCOUNTS", requestTime: requestTime });
    },
    requestUpdateEnabled: (requestTime: number, accountId: number, value: boolean): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (getState().accounts.accountStates[accountId]
            && requestTime === getState().accounts.accountStates[accountId].requestTime)
            return;

        let api = new Api.AccountApi();
        let task = api.apiAccountUpdateAccountEnabledGet({
                accountId: accountId,
                value: value
            }, { credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(() => {
                dispatch({
                    type: "RECEIVE_UPDATE_ACCOUNT_ENABLED", requestTime: requestTime,
                    accountId: accountId, value: value
                });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({
                    type: "RECEIVE_UPDATE_ACCOUNT_ENABLED", requestTime: requestTime,
                    accountId: accountId, value: !value
                });
            });

        addTask(task);
        dispatch({
            type: "REQUEST_UPDATE_ACCOUNT_ENABLED", requestTime: requestTime,
            accountId: accountId, value: value
        });
    }
};

const unloadedState: AccountsState = {
    isLoading: false,
    requestTime: 0,
    accounts: [],
    accountStates: {}
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<AccountsState> = (state: AccountsState, incomingAction: Action) => {
    let action = incomingAction as KnownAction;
    switch (action.type) {
        case "REQUEST_ACCOUNTS":
            return {
                ...state,
                isLoading: true,
                requestTime: action.requestTime
            };
        case "RECEIVE_ACCOUNTS":
            if (state.requestTime !== action.requestTime)
                return state;

            return {
                ...state,
                isLoading: false,
                accounts: action.accounts
            };
        case "REQUEST_UPDATE_ACCOUNT_ENABLED":
            return {
                ...state,
                accountStates: {
                    ...state.accountStates,
                    [action.accountId]: {
                        ...state.accountStates[action.accountId],
                        isLoading: true,
                        requestTime: action.requestTime
                    }
                }
            };
        case "RECEIVE_UPDATE_ACCOUNT_ENABLED":
            if (state.accountStates[action.accountId]
                && state.accountStates[action.accountId].requestTime !== action.requestTime)
                return state;

            return {
                ...state,
                accounts: state.accounts
                    .map(x => x.accountId === (action as ReceiveUpdateAccountEnabled).accountId
                        ? { ...x, enabled: (action as ReceiveUpdateAccountEnabled).value }
                        : x),
                accountStates: {
                    ...state.accountStates,
                    [action.accountId]: {
                        ...state.accountStates[action.accountId],
                        isLoading: false
                    }
                }
            };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || unloadedState;
};
