﻿import { fetch, addTask } from "domain-task";
import { Action, Reducer } from 'redux';
import * as Api from "../api/api";
import * as Moment from "moment";
import { getDefaultHeaders } from "../utils/utils";
import { ApplicationState, AppThunkAction } from ".";
import { push, RouterAction } from "react-router-redux";
import { SubmissionError } from "redux-form";

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface TacticState {
    isLoading: boolean;
    requestTime: number;
    tactic: Api.TacticModel;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

interface DetailsRequestTactic { type: "DETAILS_REQUEST_TACTIC"; requestTime: number }
interface DetailsReceiveTactic { type: "DETAILS_RECEIVE_TACTIC"; requestTime: number; tactic: Api.TacticModel }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownAction = DetailsRequestTactic | DetailsReceiveTactic;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestTactic: (requestTime: number, tacticId: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (requestTime === getState().tactic.requestTime)
            return;

        let inStore = getState().map.tactics.find(x => x.tacticId === tacticId);
        if (inStore) {
            dispatch({ type: "DETAILS_REQUEST_TACTIC", requestTime: requestTime });
            dispatch({ type: "DETAILS_RECEIVE_TACTIC", requestTime: requestTime, tactic: inStore });
            return;
        }

        let api = new Api.TacticApi();
        let task = api.apiTacticGetTacticByIdGet({
            id: tacticId
        }, { credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(tactic => {
                dispatch({ type: "DETAILS_RECEIVE_TACTIC", requestTime: requestTime, tactic: tactic });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "DETAILS_RECEIVE_TACTIC", requestTime: requestTime, tactic: null });
            });

        addTask(task);
        dispatch({ type: "DETAILS_REQUEST_TACTIC", requestTime: requestTime });
    }
};

const unloadedState: TacticState = {
    isLoading: false,
    requestTime: 0,
    tactic: null
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<TacticState> = (state: TacticState, incomingAction: Action) => {
    let action = incomingAction as KnownAction;
    switch (action.type) {
        case "DETAILS_REQUEST_TACTIC":
            return {
                ...state,
                isLoading: true,
                requestTime: action.requestTime
            };
        case "DETAILS_RECEIVE_TACTIC":
            if (state.requestTime !== action.requestTime)
                return state;

            return {
                ...state,
                isLoading: false,
                tactic: action.tactic
            };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || unloadedState;
};
