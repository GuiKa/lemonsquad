﻿import { fetch, addTask } from "domain-task";
import { Action, Reducer } from 'redux';
import * as Api from "../api/api";
import * as Moment from "moment";
import { getDefaultHeaders } from "../utils/utils";
import { ApplicationState, AppThunkAction } from ".";
import { push, RouterAction } from "react-router-redux";
import { SubmissionError } from "redux-form";
import { ReceiveEditTactic } from "./EditTactic";

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface AccountTokensState {
    isLoading: boolean;
    requestTime: number;
    accountTokens: Array<Api.AccountTokenModel>;
    accountTokenStates: { [id: number]: AccountTokenRequestState };
    createState: AccountTokenRequestState;
}

interface AccountTokenRequestState {
    isLoading: boolean;
    requestTime: number;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

interface RequestAccountTokens { type: "REQUEST_ACCOUNT_TOKENS"; requestTime: number }
interface ReceiveAccountTokens { type: "RECEIVE_ACCOUNT_TOKENS"; requestTime: number; accountTokens: Array<Api.AccountTokenModel> }

interface RequestDeleteAccountToken { type: "REQUEST_DELETE_ACCOUNT_TOKEN"; requestTime: number; accountTokenId: number }
interface ReceiveDeleteAccountToken { type: "RECEIVE_DELETE_ACCOUNT_TOKEN"; requestTime: number; accountTokenId: number; success: boolean }

interface RequestCreateAccountToken { type: "REQUEST_CREATE_ACCOUNT_TOKEN"; requestTime: number; }
interface ReceiveCreateAccountToken { type: "RECEIVE_CREATE_ACCOUNT_TOKEN"; requestTime: number; accountToken: Api.AccountTokenModel }


// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownAction = RequestAccountTokens | ReceiveAccountTokens
    | RequestDeleteAccountToken | ReceiveDeleteAccountToken
    | RequestCreateAccountToken | ReceiveCreateAccountToken;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestAccountTokens: (requestTime: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (requestTime === getState().accountTokens.requestTime)
            return;

        let api = new Api.AccountTokenApi();
        let task = api.apiAccountTokenGetAccountTokensGet({ credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(accountTokens => {
                dispatch({ type: "RECEIVE_ACCOUNT_TOKENS", requestTime: requestTime, accountTokens: accountTokens });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "RECEIVE_ACCOUNT_TOKENS", requestTime: requestTime, accountTokens: [] });
            });

        addTask(task);
        dispatch({ type: "REQUEST_ACCOUNT_TOKENS", requestTime: requestTime });
    },
    requestDeleteAccountToken: (requestTime: number, accountTokenId: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (getState().accountTokens.accountTokenStates[accountTokenId]
            && requestTime === getState().accountTokens.accountTokenStates[accountTokenId].requestTime)
            return;

        let api = new Api.AccountTokenApi();
        let task = api.apiAccountTokenDeleteGet({
            id: accountTokenId
        }, { credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(accountTokens => {
                dispatch({ type: "RECEIVE_DELETE_ACCOUNT_TOKEN", requestTime: requestTime, accountTokenId: accountTokenId, success: true  });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "RECEIVE_DELETE_ACCOUNT_TOKEN", requestTime: requestTime, accountTokenId: accountTokenId, success: false });
            });

        addTask(task);
        dispatch({ type: "REQUEST_DELETE_ACCOUNT_TOKEN", requestTime: requestTime, accountTokenId: accountTokenId });
    },
    requestCreateAccountToken: (requestTime: number, accountToken: Api.AccountTokenModel): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (requestTime === getState().accountTokens.requestTime)
            return;

        let api = new Api.AccountTokenApi();
        let task = api.apiAccountTokenCreatePost({
            model: accountToken
        }, { credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(accountToken => {
                dispatch({ type: "RECEIVE_CREATE_ACCOUNT_TOKEN", requestTime: requestTime, accountToken: accountToken });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "RECEIVE_CREATE_ACCOUNT_TOKEN", requestTime: requestTime, accountToken: null });
            });

        addTask(task);
        dispatch({ type: "REQUEST_CREATE_ACCOUNT_TOKEN", requestTime: requestTime });
    },
};

const unloadedState: AccountTokensState = {
    isLoading: false,
    requestTime: 0,
    accountTokens: [],
    accountTokenStates: {},
    createState: {
        isLoading: false,
        requestTime: 0
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<AccountTokensState> = (state: AccountTokensState, incomingAction: Action) => {
    let action = incomingAction as KnownAction;
    switch (action.type) {
        case "REQUEST_ACCOUNT_TOKENS":
            return {
                ...state,
                isLoading: true,
                requestTime: action.requestTime
            };
        case "RECEIVE_ACCOUNT_TOKENS":
            if (state.requestTime !== action.requestTime)
                return state;

            return {
                ...state,
                isLoading: false,
                accountTokens: action.accountTokens
            };
        case "REQUEST_DELETE_ACCOUNT_TOKEN":
            return {
                ...state,
                accountTokenStates: {
                    ...state.accountTokenStates,
                    [action.accountTokenId]: {
                        ...state.accountTokenStates[action.accountTokenId],
                        isLoading: true,
                        requestTime: action.requestTime
                    }
                }
            };
        case "RECEIVE_DELETE_ACCOUNT_TOKEN":
            return {
                ...state,
                accountTokens: state.accountTokens
                    .filter(x => !(action as ReceiveDeleteAccountToken).success
                        || x.accountTokenId !== (action as ReceiveDeleteAccountToken).accountTokenId),
                accountTokenStates: {
                    ...state.accountTokenStates,
                    [action.accountTokenId]: {
                        ...state.accountTokenStates[action.accountTokenId],
                        //Only the last reception will unload
                        isLoading: state.accountTokenStates[action.accountTokenId]
                            && state.accountTokenStates[action.accountTokenId].requestTime === action.requestTime
                            ? false : state.accountTokenStates[action.accountTokenId].isLoading
                    }
                }
            };
        case "REQUEST_CREATE_ACCOUNT_TOKEN":
            return {
                ...state,
                createState: {
                    ...state.createState,
                    isLoading: true,
                    requestTime: action.requestTime
                }
            };
        case "RECEIVE_CREATE_ACCOUNT_TOKEN":
            return {
                ...state,
                accountTokens: state.accountTokens.concat([action.accountToken].filter(x => x)),
                createState: {
                    ...state.createState,
                    isLoading: state.createState.requestTime === action.requestTime
                        ? false : state.createState.isLoading
                }
            };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || unloadedState;
};
