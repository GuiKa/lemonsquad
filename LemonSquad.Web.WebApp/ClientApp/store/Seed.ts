﻿import { fetch, addTask } from "domain-task";
import { Action, Reducer } from 'redux';
import * as Api from "../api/api";
import * as Moment from "moment";
import { getDefaultHeaders } from "../utils/utils";
import { ApplicationState, AppThunkAction } from ".";

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface SeedState {
    isLoading: boolean;
    isLoaded: boolean;
    requestTime: number;
    seed: Api.SeedModel;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.
interface RequestSeed { type: 'REQUEST_SEED'; requestTime: number }
interface ReceiveSeed { type: 'RECEIVE_SEED'; requestTime: number; seed: Api.SeedModel }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownAction = RequestSeed | ReceiveSeed;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const requestSeed = (requestTime: number, dispatch: (action: KnownAction) => void, getState: () => ApplicationState): Promise<any> => {
    if (requestTime === getState().seed.requestTime)
        return Promise.resolve();

    //Do not redownload if not old
    //Not sure if needed
    if (requestTime < new Date().getTime() - 1000 * 60)
        return Promise.resolve();

    let api = new Api.SeedApi();
    let fetchTask = api.apiSeedGetSeedGet().then(seed => {
        dispatch({
            type: "RECEIVE_SEED",
            requestTime: requestTime,
            seed: seed
        });
    }).catch(error => {
        console.log("Error fetching seed: " + error.message);
        dispatch({
            type: "RECEIVE_SEED",
            requestTime: requestTime,
            seed: {
                maps: {},
                mapUplinks: {}
            }
        });
    });

    //Tell the server to wait for this promise to end
    addTask(fetchTask);
    dispatch({ type: "REQUEST_SEED", requestTime: requestTime });
    return fetchTask;
}

export const actionCreators = {
    requestSeed: (requestTime: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        return requestSeed(requestTime, dispatch, getState);
    }
};

const unloadedState: SeedState = {
    isLoading: false,
    isLoaded: false,
    requestTime: 0,
    seed: null
};
// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<SeedState> = (state: SeedState, incomingAction: Action) => {
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_SEED':
            return {
                ...state,
                isLoading: true,
                requestTime: action.requestTime
            };
        case 'RECEIVE_SEED':
            if (action.requestTime !== state.requestTime)
                return state;

            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                seed: action.seed
            };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || unloadedState;
};
