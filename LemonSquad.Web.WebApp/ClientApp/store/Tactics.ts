﻿import { fetch, addTask } from "domain-task";
import { Action, Reducer } from 'redux';
import * as Api from "../api/api";
import * as Moment from "moment";
import { getDefaultHeaders } from "../utils/utils";
import { ApplicationState, AppThunkAction } from ".";
import { push, RouterAction } from "react-router-redux";
import { SubmissionError } from "redux-form";
import { ReceiveEditTactic } from "./EditTactic";

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface TacticsState {
    isLoading: boolean;
    requestTime: number;
    tactics: Array<Api.TacticModel>;
    tacticStates: { [id: number]: TacticState };
    createTacticState: CreateTacticState;
}

export interface TacticState {
    isLoading: boolean;
    requestTime: number;
}

interface CreateTacticState {
    isLoading: boolean;
    requestTime: number;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

interface RequestTactics { type: "REQUEST_ALL_TACTICS"; requestTime: number }
interface ReceiveTactics { type: "RECEIVE_ALL_TACTICS"; requestTime: number; tactics: Array<Api.TacticModel> }


// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownAction = RequestTactics | ReceiveTactics
    | ReceiveEditTactic;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestTactics: (requestTime: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (requestTime === getState().tactics.requestTime)
            return;

        let api = new Api.TacticApi();
        let task = api.apiTacticGetAllTacticsGet({ credentials: "same-origin", headers: getDefaultHeaders(getState()) })
            .then(tactics => {
                dispatch({ type: "RECEIVE_ALL_TACTICS", requestTime: requestTime, tactics: tactics });
            })
            .catch(error => {
                console.log(error.message || error);
                dispatch({ type: "RECEIVE_ALL_TACTICS", requestTime: requestTime, tactics: [] });
            });

        addTask(task);
        dispatch({ type: "REQUEST_ALL_TACTICS", requestTime: requestTime });
    },
    goToTactic: (tacticId: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch(push('/tactic/' + tacticId) as any);
    }
};

const unloadedState: TacticsState = {
    isLoading: false,
    requestTime: 0,
    tactics: [],
    tacticStates: {},
    createTacticState: {
        isLoading: false,
        requestTime: 0
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<TacticsState> = (state: TacticsState, incomingAction: Action) => {
    let action = incomingAction as KnownAction;
    switch (action.type) {
        case "REQUEST_ALL_TACTICS":
            return {
                ...state,
                isLoading: true,
                requestTime: action.requestTime
            };
        case "RECEIVE_ALL_TACTICS":
            if (state.requestTime !== action.requestTime)
                return state;

            return {
                ...state,
                isLoading: false,
                tactics: action.tactics
            };
        case "RECEIVE_EDIT_TACTIC":
            return {
                ...state,
                tactics: state.tactics
                    .map(x => x.tacticId === (action as ReceiveEditTactic).tactic.tacticId
                        ? {
                            ...(action as ReceiveEditTactic).tactic,
                            account: x.account,
                        }
                        : x)
            };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || unloadedState;
};
