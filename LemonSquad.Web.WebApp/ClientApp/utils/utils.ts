﻿import * as Api from '../api/api';
import { ApplicationState } from "../store/index"


export const getDefaultHeaders = (state: ApplicationState): any => {
    return {
        Authorization: 'Bearer ' + state.account.token
    };
}

export const dictionnaryToArray = <T>(entities: { [id: number]: T }): Array<T> => {
    let array = [];
    for (let id of Object.keys(entities)) {
        array.push(entities[Number.parseInt(id)]);
    }
    return array;
}