import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import Home from './components/Home';
import Login from './components/Login';
import Maps from './components/Maps';
import MapDetails from './components/MapDetails';
import Register from './components/Register';
import MyTactics from './components/MyTactics';
import EditTactic from './components/EditTactic';
import TacticDetails from './components/TacticDetails';
import Accounts from './components/Accounts';
import Tokens from './components/Tokens';
import Tactics from './components/Tactics';
import UserIsAuthenticated from './security/UserIsAuthenticated';
import UserIsNotAuthenticated from './security/UserIsNotAuthenticated';

export const routes = <Layout>
    <Route exact path='/' component={ Home } />
    <Route path="/register" component={UserIsNotAuthenticated(Register)} />
    <Route path="/login" component={UserIsNotAuthenticated(Login)} />
    <Route path="/maps" component={UserIsAuthenticated(Maps)} />
    <Route path="/map/:mapId" component={UserIsAuthenticated(MapDetails)} />
    <Route path="/mytactics" component={UserIsAuthenticated(MyTactics)} />
    <Route path="/editTactic/:tacticId" component={UserIsAuthenticated(EditTactic)} />
    <Route path="/tactic/:tacticId" component={UserIsAuthenticated(TacticDetails)} />
    <Route path="/accounts" component={UserIsAuthenticated(Accounts)} />
    <Route path="/tokens" component={UserIsAuthenticated(Tokens)} />
    <Route path="/tactics" component={UserIsAuthenticated(Tactics)} />
</Layout>;
