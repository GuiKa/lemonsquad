﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LemonSquad.Context;
using LemSquad.Web.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LemonSquad.Web.WebApp.Controllers
{
    [Produces("application/json")]
    public abstract class BaseCrudeController<TEntity, TEntityModel> : Controller
        where TEntity: class, new() 
        where TEntityModel: class, new()
    {
        protected readonly LsContext Db;
        protected readonly IMapper Mapper;
        protected readonly IIdentityContext IdentityContext;

        protected BaseCrudeController(LsContext db,
            IIdentityContext identityContext,
            IMapper mapper)
        {
            Db = db;
            IdentityContext = identityContext;
            Mapper = mapper;
        }

        [HttpPost("[action]")]
        public TEntityModel Create([FromBody] TEntityModel model)
        {
            var entity = new TEntity();
            ApplyModel(entity, model);
            OnCreate(entity);
            DbSet.Add(entity);
            Db.SaveChanges();

            return ToModel(
                GetEntityByEntity(GetEntities(), entity));
        }

        [HttpPost("[action]")]
        public TEntityModel Edit([FromBody] TEntityModel model)
        {
            if (!CanEdit)
                return null;

            var entity = GetEntityByModel(GetEntities(), model);
            ApplyModel(entity, model);
            Db.SaveChanges();

            return ToModel(entity);
        }

        [HttpGet("[action]")]
        public IActionResult Delete([FromQuery] int id)
        {
            if (!CanDelete)
                return null;

            var entity = GetEntityById(GetEntities(), id);
            if (!BeforeRemove(entity))
                return Forbid();

            DbSet.Remove(entity);
            Db.SaveChanges();

            return Ok();
        }

        protected virtual bool CanEdit => true;
        protected virtual bool CanDelete => true;

        protected abstract IQueryable<TEntity> GetEntities();
        protected abstract DbSet<TEntity> DbSet { get; }
        protected abstract TEntity GetEntityByEntity(IQueryable<TEntity> entities, TEntity entity);
        protected abstract TEntity GetEntityByModel(IQueryable<TEntity> entities, TEntityModel model);
        protected abstract TEntity GetEntityById(IQueryable<TEntity> entities, int id);

        protected virtual void OnCreate(TEntity entity)
        {
        }

        protected virtual bool BeforeRemove(TEntity entity)
        {
            return true;
        }

        protected virtual void ApplyModel(TEntity entity, TEntityModel model)
        {
            Mapper.Map(model, entity);
        }

        protected virtual TEntityModel ToModel(TEntity entity)
        {
            return Mapper.Map<TEntityModel>(entity);
        }
    }
}