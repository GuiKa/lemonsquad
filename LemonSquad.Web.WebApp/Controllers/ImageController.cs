﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LemonSquad.Context;
using LemonSquad.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using io = System.IO;
using LemonSquad.Web.Models;
using LemonSquad.Web.WebApp.Utility;
using Microsoft.AspNetCore.Authorization;

namespace LemonSquad.Web.WebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Image")]
    public class ImageController : Controller
    {
        private const string notFound = "notfound.jpg";
        private string[] extensions = new []{ "jpg", "png", "gif", "svg" };

        private readonly IHostingEnvironment _environment;
        private readonly LsContext _db;
        private readonly IMapper _mapper;

        public ImageController(IHostingEnvironment environment,
            IMapper mapper,
            LsContext db)
        {
            _environment = environment;
            _db = db;
            _mapper = mapper;
        }

        [HttpGet("[action]")]
        public FileResult GetMapImage([FromQuery]string code, [FromQuery]bool thumb = false)
        {
            var fileName = extensions
                .Select(x => io.Path.Combine(_environment.WebRootPath, $@"images\{code}{(thumb ? "_tm": "")}.{x}"))
                .FirstOrDefault(x => io.File.Exists(x))
                ?? io.Path.Combine(_environment.WebRootPath, $@"images\{notFound}");

            return PhysicalFile(fileName, $"image/{io.Path.GetExtension(fileName).Replace(".", "")}");
        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<ImageModel> Create([FromForm]IFormFile file)
        {
            if (file.Length / 1024 / 1024 > 2)
                return null;

            var image = new Image
            {
                Extension = io.Path.GetExtension(file.FileName).Replace(".", "")
            };
            using (var stream = new io.MemoryStream())
            {
                await file.CopyToAsync(stream);
                image.Content = stream.ToArray();
                image.Hash = Hashing.Md5Hash(image.Content);
            }

            var existingImage = _db.Images.FirstOrDefault(x => x.Hash == image.Hash);
            if (existingImage != null)
                return _mapper.Map<ImageModel>(existingImage);

            _db.Images.Add(image);
            await _db.SaveChangesAsync();
            return _mapper.Map<ImageModel>(image);
        }

        [HttpGet("[action]")]
        public FileResult GetImageContent([FromQuery]int imageId)
        {
            var image = _db.Images.Find(imageId);
            if(image == null)
                return PhysicalFile(
                    io.Path.Combine(_environment.WebRootPath, $@"images\{notFound}"), 
                    $"image/{io.Path.GetExtension(notFound).Replace(".", "")}");

            return File(image.Content,
                $"image/{image.Extension}",
                $"{image.ImageId}.{image.Extension}");
        }
    }
}