﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using LemonSquad.Context;
using LemonSquad.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LemonSquad.Web.WebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Seed")]
    public class SeedController : Controller
    {
        private readonly LsContext _db;
        private readonly IMapper _mapper;

        public SeedController(LsContext db,
            IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        [HttpGet("[action]")]
        public async Task<SeedModel> GetSeed()
        {
            var maps = _db.Maps.ProjectTo<MapModel>()
                .ToDictionaryAsync(x => x.MapId, x => x);
            var mapUplinks = _db.MapUplinks.ProjectTo<MapUplinkModel>()
                .ToDictionaryAsync(x => x.MapUplinkId, x => x);

            return new SeedModel
            {
                Maps = await maps,
                MapUplinks = await mapUplinks
            };
        }
    }
}