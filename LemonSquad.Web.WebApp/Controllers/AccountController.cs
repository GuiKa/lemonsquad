﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using LemonSquad.Context;
using LemonSquad.Models;
using LemonSquad.Web.Models;
using LemonSquad.Web.WebApp.Filter;
using LemonSquad.Web.WebApp.Security;
using LemSquad.Web.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace LemonSquad.Web.WebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly LsContext _db;
        private readonly IMapper _mapper;
        private readonly IIdentityContext _identityContext;
        private readonly TokenProvider _tokenProvider;
        private readonly LoginSettings _loginSettings;
        private readonly JwtOptions _jwtOptions;

        public AccountController(LsContext db,
            IOptions<LoginSettings> loginSettings,
            IOptions<JwtOptions> jwtOptions,
            IIdentityContext identityContex,
            TokenProvider tokenProvider,
            IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
            _loginSettings = loginSettings.Value;
            _jwtOptions = jwtOptions.Value;
            _identityContext = identityContex;
            _tokenProvider = tokenProvider;

        }

        [HttpGet("[action]")]
        public async Task<CurrentUserModel> GetCurrentUser()
        {
            var account = await _db.Accounts
                .FirstOrDefaultAsync(x => x.Username == _identityContext.Username);

            if(account == null)
                return new CurrentUserModel();

            return new CurrentUserModel
            {
                Account = _mapper.Map<AccountModel>(account),
                ExpirationDate = _identityContext.ExpirationDate,
                Roles = _db.Roles
                    .Where(x => _identityContext.Roles.Contains(x.Name))
                    .ProjectTo<RoleModel>().ToArray()
            };
        }

        [HttpPost("[action]")]
        public async Task<TokenResultModel> Register([FromBody]RegisterModel model)
        {
            if(_db.Accounts.Any(x => x.Username == model.Username))
                return new TokenResultModel { Message = "Username taken" };

            var tokenLimit = DateTime.UtcNow.AddDays(-2);
            var token = await _db.AccountTokens
                .FirstOrDefaultAsync(x =>
                    x.AccountId == null
                    && x.Type == TokenType.Register
                    && tokenLimit <= x.CreationDate
                    && x.Value == model.Token);

            if(token == null)
                return new TokenResultModel { Message = "Invalid token" };

            var account = new Account
            {
                Username = model.Username,
                Password = LsIdentityProvider.HashPassword(model.Password, _loginSettings.Salt)
            };
            if (token.Admin)
            {
                account.AccountRoles = _db.Roles.Select(x => new AccountRole
                {
                    RoleId = x.RoleId
                }).ToList();
            }
            await _db.Accounts.AddAsync(account);
            token.Account = account;
            await _db.SaveChangesAsync();

            return await CreateToken(new LoginModel
            {
                Username = model.Username,
                Password = model.Password
            });
        }

        [HttpPost("[action]")]
        public async Task<TokenResultModel> CreateToken([FromBody]LoginModel model)
        {
            var tokenResult = await _tokenProvider.ProvideToken(model, _loginSettings.TokenTtl);

            if (tokenResult.Token != null)
                Response.Cookies.Append(_jwtOptions.CookieName, tokenResult.Token.Token);

            return tokenResult;
        }

        [Authorize]
        [HttpGet("[action]")]
        [HasRolesFilterFactory(Roles = new []{ "Admin" })]
        public IEnumerable<AccountModel> GetAccounts()
        {
            return _db.Accounts
                .ProjectTo<AccountModel>();
        }

        [Authorize]
        [HttpGet("[action]")]
        [HasRolesFilterFactory(Roles = new[] { "Admin" })]
        public IActionResult UpdateAccountEnabled([FromQuery]int accountId, [FromQuery]bool value)
        {
            var account = _db.Accounts.Find(accountId);
            account.Enabled = value;
            _db.SaveChanges();
            return Ok();
        }
    }
}