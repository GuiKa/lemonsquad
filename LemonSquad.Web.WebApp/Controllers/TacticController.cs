﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using LemonSquad.Context;
using LemonSquad.Models;
using LemonSquad.Web.Models;
using LemSquad.Web.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LemonSquad.Web.WebApp.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class TacticController: BaseCrudeController<Tactic, TacticModel>
    {
        public TacticController(LsContext db, IIdentityContext identityContext, IMapper mapper) 
            : base(db, identityContext, mapper)
        {
        }

        [HttpGet("[action]")]
        public IEnumerable<TacticModel> GetMapTactics([FromQuery]int mapId)
        {
            return GetEntities()
                .Where(x => x.MapUplink.MapId == mapId
                            && x.Status == TacticStatus.Opened)
                .ProjectTo<TacticModel>();
        }

        [HttpGet("[action]")]
        public IEnumerable<TacticModel> GetMyTactics()
        {
            return GetEntities()
                .Where(x => x.Account.Username == IdentityContext.Username)
                .ProjectTo<TacticModel>();
        }

        [HttpGet("[action]")]
        public IEnumerable<TacticModel> GetAllTactics()
        {
            return GetEntities()
                .Where(x => x.Status == TacticStatus.Opened)
                .ProjectTo<TacticModel>();
        }

        [AllowAnonymous]
        [HttpGet("[action]")]
        public IEnumerable<TacticReference> GetTacticsReference()
        {
            return GetEntities()
                .Where(x => x.Status == TacticStatus.Opened)
                .Select(x => new TacticReference
                {
                    Name = x.Title,
                    MapName = x.MapUplink.Map.Name
                });
        }

        [HttpGet("[action]")]
        public TacticModel GetTacticById([FromQuery]int id)
        {
            return Mapper.Map<TacticModel>(GetEntities()
                .FirstOrDefault(x => x.TacticId == id));
        }

        protected override void OnCreate(Tactic entity)
        {
            entity.AccountId = Db.Accounts
                .Where(x => x.Username == IdentityContext.Username)
                .Select(x => x.AccountId).First();
        }

        protected override void ApplyModel(Tactic entity, TacticModel model)
        {
            Db.TacticContents.RemoveRange(entity.TacticContents);

            foreach (var tacticContent in model.TacticContents)
                entity.TacticContents.Add(Mapper.Map<TacticContent>(tacticContent));

            base.ApplyModel(entity, model);
        }

        protected override IQueryable<Tactic> GetEntities()
        {
            return DbSet
                .Include(x => x.MapUplink)
                .Include(x => x.TacticContents)
                .Include(x => x.Account);
        }

        protected override DbSet<Tactic> DbSet => Db.Tactics;

        protected override Tactic GetEntityByEntity(IQueryable<Tactic> entities, Tactic entity)
        {
            return entities
                .FirstOrDefault(x => x.TacticId == entity.TacticId
                                     && x.Account.Username == IdentityContext.Username);
        }

        protected override Tactic GetEntityByModel(IQueryable<Tactic> entities, TacticModel model)
        {
            return entities
                .FirstOrDefault(x => x.TacticId == model.TacticId
                                     && x.Account.Username == IdentityContext.Username);
        }

        protected override Tactic GetEntityById(IQueryable<Tactic> entities, int id)
        {
            return entities
                .FirstOrDefault(x => x.TacticId == id
                                     && x.Account.Username == IdentityContext.Username);
        }
    }
}