﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LemonSquad.Context;
using LemonSquad.Models;
using LemonSquad.Web.Models;
using LemSquad.Web.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LemonSquad.Web.WebApp.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/TacticComment")]
    public class TacticCommentController : BaseCrudeController<TacticComment, TacticCommentModel>
    {
        public TacticCommentController(LsContext db, IIdentityContext identityContext, IMapper mapper) : base(db, identityContext, mapper)
        {
        }

        protected override IQueryable<TacticComment> GetEntities()
        {
            return DbSet
                .Include(x => x.Account);
        }

        protected override DbSet<TacticComment> DbSet => Db.TacticComments;

        protected override TacticComment GetEntityByEntity(IQueryable<TacticComment> entities, TacticComment entity)
        {
            return entities
                .FirstOrDefault(x => x.TacticCommentId == entity.TacticCommentId
                                     && x.Account.Username == IdentityContext.Username);
        }

        protected override TacticComment GetEntityByModel(IQueryable<TacticComment> entities, TacticCommentModel model)
        {
            return entities
                .FirstOrDefault(x => x.TacticCommentId == model.TacticCommentId
                                     && x.Account.Username == IdentityContext.Username);
        }

        protected override TacticComment GetEntityById(IQueryable<TacticComment> entities, int id)
        {
            return entities
                .FirstOrDefault(x => x.TacticCommentId == id
                                     && x.Account.Username == IdentityContext.Username);
        }

        protected override bool CanEdit => false;
        protected override bool CanDelete => false;
    }
}