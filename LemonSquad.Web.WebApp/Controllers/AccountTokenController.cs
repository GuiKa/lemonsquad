﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using LemonSquad.Context;
using LemonSquad.Models;
using LemonSquad.Web.Models;
using LemonSquad.Web.WebApp.Filter;
using LemSquad.Web.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LemonSquad.Web.WebApp.Controllers
{
    [Authorize]
    [HasRolesFilterFactory(Roles = new []{ "Admin" })]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AccountTokenController : BaseCrudeController<AccountToken, AccountTokenModel>
    {
        public AccountTokenController(LsContext db, IIdentityContext identityContext, IMapper mapper) : base(db, identityContext, mapper)
        {
        }

        [HttpGet("[action]")]
        public IEnumerable<AccountTokenModel> GetAccountTokens()
        {
            return Db.AccountTokens
                .ProjectTo<AccountTokenModel>();
        }

        protected override bool BeforeRemove(AccountToken entity)
        {
            if (entity.AccountId != null)
                return false;

            return true;
        }

        protected override IQueryable<AccountToken> GetEntities()
        {
            return DbSet.Where(x => x.AccountId == null);
        }

        protected override bool CanEdit => false;

        protected override DbSet<AccountToken> DbSet => Db.AccountTokens;

        protected override AccountToken GetEntityByEntity(IQueryable<AccountToken> entities, AccountToken entity)
        {
            return entities.FirstOrDefault(x => x.AccountTokenId == entity.AccountTokenId);
        }

        protected override AccountToken GetEntityByModel(IQueryable<AccountToken> entities, AccountTokenModel model)
        {
            return entities.FirstOrDefault(x => x.AccountTokenId == model.AccountTokenId);
        }

        protected override AccountToken GetEntityById(IQueryable<AccountToken> entities, int id)
        {
            return entities.FirstOrDefault(x => x.AccountTokenId == id);
        }
    }
}