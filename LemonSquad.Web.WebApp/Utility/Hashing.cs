﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace LemonSquad.Web.WebApp.Utility
{
    public class Hashing
    {
        public static string Md5Hash(byte[] data)
        {
            using (var md5 = MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(data)).Replace("-", "");
            }
        }
    }
}
